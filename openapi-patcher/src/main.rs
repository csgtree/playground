use std::env;
use std::fs::File;

fn main() {
    if env::args().len() != 2 {
        eprintln!("Usage: openapi-patcher OPENAPI-YAML-FILE...");
        std::process::exit(1);
    }

    let args: Vec<String> = env::args().collect();
    let openapi_yaml_path = &args[1];

    let openapi_yaml_file = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .open(openapi_yaml_path)
        .expect("File should exist");
    use serde_yaml::Value;
    let mut openapi_value: Value = serde_yaml::from_reader(&openapi_yaml_file).unwrap();

    let login_api: serde_yaml::Value = serde_yaml::from_str(
        "
    post:
      description: |
        Operation ID: generateRefreshToken<br>
        Login to get refresh token.
      operationId: generate_refresh_token
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginRequest'
        required: false
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
          description: OK
      security: [ ]
      summary: Refresh JWT access token with refresh token
      tags:
        - token
        - required
    ",
    )
    .unwrap();
    openapi_value["paths"]["/api/v1/auth/login"] = login_api;

    let login_req: serde_yaml::Value = serde_yaml::from_str(
        "
      properties:
        username:
          description: The username of the adaptor
          example: admin
          title: username
          type: string
        password:
          description: The password of the adaptor
          example: secret
          title: password
          type: string
      required:
        - username
      title: login request
      type: object
    ",
    )
    .unwrap();
    openapi_value["components"]["schemas"]["LoginRequest"] = login_req;

    let login_rsp: serde_yaml::Value = serde_yaml::from_str(
        "
      properties:
        refresh_token:
          description: The refresh token of the adaptor
          title: refresh_token
          type: string
      required:
        - refresh_token
      title: login response
      type: object
    ",
    )
    .unwrap();
    openapi_value["components"]["schemas"]["LoginResponse"] = login_rsp;

    let output_file = File::create("./patched-osis-openapi.yaml").expect("File should exist");
    serde_yaml::to_writer(output_file, &openapi_value).unwrap();
}
