-- Pull in the wezterm API
-- local lm = require("utils/launch_menu")
local wezterm = require("wezterm")
local act = wezterm.action
local mux = wezterm.mux

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:

-- color scheme
-- config.color_scheme = "Gruvbox Dark (Gogh)"
-- config.color_scheme = "TokyoNightLight (Gogh)"
-- config.color_scheme = "Batman"
-- config.color_scheme = "Catppuccin Macchiato"
-- config.color_scheme = "Atom"
-- config.color_scheme = "GruvboxDark"
-- config.color_scheme = "Sonokai (Gogh)"
-- config.color_scheme = "GruvboxDarkHard"
config.color_scheme = "tokyonight"

-- font
config.font = wezterm.font({
	family = "MonoLisa",
	-- family = "JetBrains Mono",
	weight = "Regular",
	-- weight = "Light",
	-- stretch = "Expanded",
	harfbuzz_features = { "zero", "calt=1", "clig=1", "liga=1" },
})

config.font_size = 17.0
config.line_height = 1.0

-- cursor
config.default_cursor_style = "BlinkingBlock"

-- window
config.window_padding = {
	left = 5,
	right = 5,
	top = 0,
	bottom = 0,
}
config.window_decorations = "NONE"
config.window_background_opacity = 0.925 -- personal recommended value

-- tab
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true
config.hide_tab_bar_if_only_one_tab = true
config.switch_to_last_active_tab_when_closing_tab = true
config.tab_max_width = 32

--- env vars
config.set_environment_variables = {
	TERM = "xterm-256color",
}

-- general
config.adjust_window_size_when_changing_font_size = true
config.debug_key_events = false
-- config.enable_tab_bar = false
config.native_macos_fullscreen_mode = true
config.window_close_confirmation = "NeverPrompt"
config.window_decorations = "RESIZE"

-- ssh
config.ssh_domains = {
	{
		-- This name identifies the domain
		name = "apaas-167",
		-- The hostname or address to connect to. Will be used to match settings
		-- from your ssh config file
		remote_address = "10.148.91.167",
		-- The username to use on the remote host
		username = "root",
	},
}

-- launch
-- config.launch_menu = lm

-- keys

config.keys = {

	-- activate command palette
	{
		key = "P",
		mods = "SHIFT|CMD",
		action = wezterm.action.ActivateCommandPalette,
	},
}

-- Prompting for the workspace name
wezterm.on("update-right-status", function(window, pane)
	window:set_right_status(window:active_workspace())
end)

-- and finally, return the configuration to wezterm
return config
