local wezterm = require("wezterm")

local launch_menu = {}
table.insert(launch_menu, {
	label = "top",
	args = { "top" },
})

local ssh_config_file = wezterm.home_dir .. "/.ssh/config"
local f = io.open(ssh_config_file)
if f then
	local line = f:read("*l")
	while line do
		if line:find("Host ") == 1 then
			local host = line:gsub("Host ", "")
			table.insert(launch_menu, {
				label = "SSH Wangc" .. host,
				args = { "ssh", host },
			})
		end
		line = f:read("*l")
	end
	f:close()
end

return launch_menu
