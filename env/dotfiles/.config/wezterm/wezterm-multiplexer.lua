-- Pull in the wezterm API
-- local lm = require("utils/launch_menu")
local wezterm = require("wezterm")
local act = wezterm.action
local mux = wezterm.mux

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- session manager
wezterm.on("save_session", function(window)
	session_manager.save_state(window)
end)
wezterm.on("load_session", function(window)
	session_manager.load_state(window)
end)
wezterm.on("restore_session", function(window)
	session_manager.restore_state(window)
end)

-- For example, changing the color scheme:

-- color scheme
-- config.color_scheme = "Gruvbox Dark (Gogh)"
-- config.color_scheme = "TokyoNightLight (Gogh)"
-- config.color_scheme = "Batman"
-- config.color_scheme = "Catppuccin Macchiato"
-- config.color_scheme = "Atom"
-- config.color_scheme = "GruvboxDark"
-- config.color_scheme = "Sonokai (Gogh)"
-- config.color_scheme = "GruvboxDarkHard"
config.color_scheme = "tokyonight"

-- font
config.font = wezterm.font({
	-- family = "MonoLisa",
	family = "IosevkaTerm Nerd Font",
	-- family = "Iosevka Nerd Font",
	-- family = "Iosevka Semi Bold Nerd Font",
	-- family = "JetBrains Mono",
	-- family = "Meslo",
	weight = "Regular",
	-- weight = "Light",
	-- stretch = "Expanded",
	harfbuzz_features = { "zero", "calt=1", "clig=1", "liga=1" },
})

config.font_size = 13.0
config.line_height = 1.0

-- cursor
config.default_cursor_style = "BlinkingBlock"

-- window
config.window_padding = {
	left = 5,
	right = 5,
	top = 0,
	bottom = 0,
}
config.window_decorations = "NONE"
config.window_background_opacity = 0.925 -- personal recommended value

-- tab
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true
config.hide_tab_bar_if_only_one_tab = true
config.switch_to_last_active_tab_when_closing_tab = true
config.tab_max_width = 32

-- inactive panes
config.inactive_pane_hsb = {
	saturation = 0.2,
	brightness = 0.25,
}
-- The filled in variant of the < symbol
local SOLID_LEFT_ARROW = wezterm.nerdfonts.pl_right_hard_divider

-- The filled in variant of the > symbol
local SOLID_RIGHT_ARROW = wezterm.nerdfonts.pl_left_hard_divider

-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
function tab_title(tab_info)
	local title = tab_info.tab_title
	-- if the tab title is explicitly set, take that
	if title and #title > 0 then
		return title
	end
	-- Otherwise, use the title from the active pane
	-- in that tab
	return tab_info.active_pane.title
end

wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
	local edge_background = "#0b0022"
	local background = "#1b1032"
	local foreground = "#808080"

	if tab.is_active then
		background = "#2b2042"
		foreground = "#c0c0c0"
	elseif hover then
		background = "#3b3052"
		foreground = "#909090"
	end

	local edge_foreground = background

	local title = tab_title(tab)

	-- ensure that the titles fit in the available space,
	-- and that we have room for the edges.
	title = wezterm.truncate_right(title, max_width - 2)

	return {
		{ Background = { Color = edge_background } },
		{ Foreground = { Color = edge_foreground } },
		{ Text = SOLID_LEFT_ARROW },
		{ Background = { Color = background } },
		{ Foreground = { Color = foreground } },
		{ Text = title },
		{ Background = { Color = edge_background } },
		{ Foreground = { Color = edge_foreground } },
		{ Text = SOLID_RIGHT_ARROW },
	}
end)
--- env vars
config.set_environment_variables = {
	TERM = "xterm-256color",
}

-- general
config.adjust_window_size_when_changing_font_size = true
config.debug_key_events = false
-- config.enable_tab_bar = false
config.native_macos_fullscreen_mode = true
config.window_close_confirmation = "NeverPrompt"
config.window_decorations = "RESIZE"

-- ssh
config.ssh_domains = {
	{
		-- This name identifies the domain
		name = "apaas-167",
		-- The hostname or address to connect to. Will be used to match settings
		-- from your ssh config file
		remote_address = "10.148.91.167",
		-- The username to use on the remote host
		username = "root",
	},
}

-- launch
-- config.launch_menu = lm

-- keys
config.leader = { key = "a", mods = "CTRL" }

config.keys = {
	-- leader key bypass
	{
		key = "a",
		mods = "LEADER|CTRL",
		action = wezterm.action.SendKey({ key = "a", mods = "CTRL" }),
	},
	-- activate copy mode or vim mode
	{
		key = "Enter",
		mods = "LEADER",
		action = wezterm.action.ActivateCopyMode,
	},

	-- navigate tab
	{ key = "h",  mods = "ALT", action = act.ActivateTabRelative(-1) },
	{ key = "l",  mods = "ALT", action = act.ActivateTabRelative(1) },
	{ key = "F9", mods = "ALT", action = act.ShowTabNavigator },

	-- active last tab
	{
		key = "o",
		mods = "LEADER",
		action = act.ActivateLastTab,
	},

	-- rename tab
	{
		key = ",",
		mods = "LEADER",
		action = act.PromptInputLine({
			description = "Enter new name for tab",
			action = wezterm.action_callback(function(window, pane, line)
				-- line will be `nil` if they hit escape without entering anything
				-- An empty string if they just hit enter
				-- Or the actual line of text they wrote
				if line then
					window:active_tab():set_title(line)
				end
			end),
		}),
	},

	-- split pane
	{
		key = "h",
		mods = "LEADER",
		action = act.SplitVertical({ domain = "CurrentPaneDomain" }),
	},
	{
		key = "v",
		mods = "LEADER",
		action = act.SplitHorizontal({ domain = "CurrentPaneDomain" }),
	},

	-- panes by direction
	{
		key = "h",
		mods = "CTRL",
		action = act.ActivatePaneDirection("Left"),
	},
	{
		key = "l",
		mods = "CTRL",
		action = act.ActivatePaneDirection("Right"),
	},
	{
		key = "k",
		mods = "CTRL",
		action = act.ActivatePaneDirection("Up"),
	},
	{
		key = "j",
		mods = "CTRL",
		action = act.ActivatePaneDirection("Down"),
	},
	-- activate pane selection mode with the default alphabet (labels are "a", "s", "d", "f" and so on)
	{ key = "8", mods = "CTRL",   action = act.PaneSelect },
	-- activate pane selection mode with numeric labels
	{
		key = "9",
		mods = "CTRL",
		action = act.PaneSelect({
			alphabet = "1234567890",
		}),
	},
	-- show the pane selection mode, but have it swap the active and selected panes
	{
		key = "0",
		mods = "CTRL",
		action = act.PaneSelect({
			mode = "SwapWithActive",
		}),
	},
	-- adjust pane size
	{
		key = "H",
		mods = "SHIFT|ALT",
		action = act.AdjustPaneSize({ "Left", 2 }),
	},
	{
		key = "J",
		mods = "SHIFT|ALT",
		action = act.AdjustPaneSize({ "Down", 2 }),
	},
	{
		key = "K",
		mods = "SHIFT|ALT",
		action = act.AdjustPaneSize({ "Up", 2 }),
	},
	{
		key = "L",
		mods = "SHIFT|ALT",
		action = act.AdjustPaneSize({ "Right", 2 }),
	},

	-- close current pane
	{
		key = "w",
		mods = "CMD",
		action = act.CloseCurrentPane({ confirm = true }),
	},
	-- toggle pane zoom state
	{
		key = "z",
		mods = "LEADER",
		action = wezterm.action.TogglePaneZoomState,
	},
	-- ket session manager
	{ key = "s", mods = "LEADER", action = wezterm.action({ EmitEvent = "save_session" }) },
	{ key = "l", mods = "LEADER", action = wezterm.action({ EmitEvent = "load_session" }) },
	{ key = "r", mods = "LEADER", action = wezterm.action({ EmitEvent = "restore_session" }) },

	-- activate command palette
	{
		key = "P",
		mods = "SHIFT|CMD",
		action = wezterm.action.ActivateCommandPalette,
	},
}

-- key activate tab
for i = 1, 9 do
	table.insert(config.keys, {
		key = tostring(i),
		mods = "LEADER",
		action = act.ActivateTab(i - 1),
	})
end

-- Prompting for the workspace name
wezterm.on("update-right-status", function(window, pane)
	window:set_right_status(window:active_workspace())
end)

table.insert(config.keys, {
	-- Prompt for a name to use for a new workspace and switch to it.
	key = "W",
	mods = "LEADER",
	action = act.PromptInputLine({
		description = wezterm.format({
			{ Attribute = { Intensity = "Bold" } },
			{ Foreground = { AnsiColor = "Fuchsia" } },
			{ Text = "Enter name for new workspace" },
		}),
		action = wezterm.action_callback(function(window, pane, line)
			-- line will be `nil` if they hit escape without entering anything
			-- An empty string if they just hit enter
			-- Or the actual line of text they wrote
			if line then
				window:perform_action(
					act.SwitchToWorkspace({
						name = line,
					}),
					pane
				)
			end
		end),
	}),
})

-- gui startup
wezterm.on("gui-startup", function(cmd)
	local home = wezterm.home_dir
	local vimwiki_dir = home .. "/Bitbucket/vimwiki"
	local tut = home .. "/Bitbucket/playground/js-tutorial"
	local k8s_pro = home .. "/Bitbucket/kubernetes-programming-with-go-by-philippe-martin"
	local rust = home .. "/Bitbucket/tut/rust"
	local ose_ui = home .. "/GitLab/ose-master/frontend/oss-ui"
	local operator_dev = home .. "/GitLab/ose-addon"
	local minio_adaptor = home .. "/GitLab/vmware-ose-osis-minio-ri"
	local vcd_py_client = home .. "/GitLab/playground/vcd-python-client"
	local dot_kube = home .. "/.kube"

	local tab_1, pane_1, window = mux.spawn_window(cmd or {
		workspace = "daily",
		cwd = vimwiki_dir,
		-- args = args,
	})
	tab_1:set_title("vimwiki")

	local tab_2, pane_2, _ = window:spawn_tab({
		cwd = tut,
	})
	tab_2:set_title("tut")
	pane_2:split({
		direction = "Bottom",
		size = 0.3,
	})

	-- local tab_3, pane_3, _ = window:spawn_tab({
	-- 	cwd = k8s_pro,
	-- })
	-- tab_3:set_title("k8s pro")
	local tab_3, pane_3, _ = window:spawn_tab({
		cwd = rust,
	})
	tab_3:set_title("rust")

	local tab_4, pane_4, _ = window:spawn_tab({
		cwd = minio_adaptor,
	})
	tab_4:set_title("minio adaptor")

	local tab_5, pane_5, _ = window:spawn_tab({
		cwd = vcd_py_client,
	})
	tab_5:set_title("vcd py client")
	pane_5:split({
		direction = "Bottom",
		size = 0.3,
	})

	local tab_6, pane_6, _ = window:spawn_tab({
		cwd = ose_ui,
	})
	tab_6:set_title("oss ui")
	pane_6:split({
		direction = "Bottom",
		size = 0.3,
	})

	local tab_7, pane_7, _ = window:spawn_tab({
		cwd = dot_kube,
	})
	tab_7:set_title("local cluster")

	local tab_8, pane_8, _ = window:spawn_tab({
		cwd = dot_kube,
	})
	tab_8:set_title("lab clusters")

	-- maximize window
	window:gui_window():maximize()
end)

-- integrate nvim
-- if you are *NOT* lazy-loading smart-splits.nvim (recommended)
local function is_vim(pane)
	-- this is set by the plugin, and unset on ExitPre in Neovim
	return pane:get_user_vars().IS_NVIM == "true"
end

-- if you *ARE* lazy-loading smart-splits.nvim (not recommended)
-- you have to use this instead, but note that this will not work
-- in all cases (e.g. over an SSH connection). Also note that
-- `pane:get_foreground_process_name()` can have high and highly variable
-- latency, so the other implementation of `is_vim()` will be more
-- performant as well.
local function is_vim(pane)
	-- This gsub is equivalent to POSIX basename(3)
	-- Given "/foo/bar" returns "bar"
	-- Given "c:\\foo\\bar" returns "bar"
	local process_name = string.gsub(pane:get_foreground_process_name(), "(.*[/\\])(.*)", "%2")
	return process_name == "nvim" or process_name == "vim"
end

local direction_keys = {
	h = "Left",
	j = "Down",
	k = "Up",
	l = "Right",
}

local function split_nav(resize_or_move, key)
	return {
		key = key,
		mods = resize_or_move == "resize" and "CTRL" or "CTRL",
		action = wezterm.action_callback(function(win, pane)
			if is_vim(pane) then
				-- pass the keys through to vim/nvim
				win:perform_action({
					SendKey = { key = key, mods = resize_or_move == "resize" and "CTRL" or "CTRL" },
				}, pane)
			else
				if resize_or_move == "resize" then
					win:perform_action({ AdjustPaneSize = { direction_keys[key], 3 } }, pane)
				else
					win:perform_action({ ActivatePaneDirection = direction_keys[key] }, pane)
				end
			end
		end),
	}
end

local keys = {
	-- move between split panes
	split_nav("move", "h"),
	split_nav("move", "j"),
	split_nav("move", "k"),
	split_nav("move", "l"),
	-- resize panes
	-- split_nav("resize", "h"),
	-- split_nav("resize", "j"),
	-- split_nav("resize", "k"),
	-- split_nav("resize", "l"),
}
for _, k in ipairs(keys) do
	table.insert(config.keys, k)
end

-- format tab title

-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
function tab_title(tab_info)
	local title = tab_info.tab_title
	-- if the tab title is explicitly set, take that
	if title and #title > 0 then
		return title .. active_pane.title
	end
	-- Otherwise, use the title from the active pane
	-- in that tab
	return tab_info.active_pane.title
end

wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
	local title = tab_title(tab)
	if tab.is_active then
		return {
			-- { Background = { Color = "blue" } },
			{ Text = " " .. title .. " " },
		}
	end
	return title
end)

-- and finally, return the configuration to wezterm
return config
