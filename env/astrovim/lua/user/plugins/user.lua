return {
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  {
    "mfussenegger/nvim-dap",
  },
  {
    "mfussenegger/nvim-dap-python",
    ft = "python",
    dependencies = {
      "mfussenegger/nvim-dap",
      "rcarriga/nvim-dap-ui",
    },
    config = function(_, opts)
      local path = "~/.local/share/nvim/mason/packages/debugpy/venv/bin/python"
      require("dap-python").setup(path)
    end,
  },
  {
    "rcarriga/nvim-dap-ui",
    dependencies = {
      "mfussenegger/nvim-dap",
    },
    config = function()
      local dap = require "dap"
      local dapui = require "dapui"
      dapui.setup()
      dap.listeners.after.event_initialized["dapui_config"] = function() dapui.open() end
      dap.listeners.before.event_terminated["dapui_config"] = function() dapui.close() end
      dap.listeners.before.event_exited["dapui_config"] = function() dapui.close() end
    end,
  },
  {
    "psliwka/vim-smoothie",
    event = "User AstroFile",
  },
  {
    "RRethy/vim-illuminate",
    event = "User AstroFile",
    opts = {},
    config = function(_, opts) require("illuminate").configure(opts) end,
  },
  {
    "catppuccin/nvim",
    optional = true,
    opts = { integrations = { illuminate = true } },
  },
  {
    { "folke/todo-comments.nvim", opts = {}, event = "User AstroFile" },
  },
  {
    "rest-nvim/rest.nvim",
    dependencies = { { "nvim-lua/plenary.nvim" } },
    opts = {
      skip_ssl_verification = true,
      stay_in_current_window_after_split = true,
      jump_to_request = true,
      -- highlight = true,
      formatters = {
        json = "jq",
        html = function(body) return vim.fn.system({ "tidy", "-i", "-q", "-" }, body) end,
      },
    },
    vim.api.nvim_set_keymap("n", "<leader>rl", "<Plug>RestNvimLast", { noremap = true, silent = true }),
  },
  {
    "mrjones2014/smart-splits.nvim",
  },
  { -- further customize the options set by the community
    "navarasu/onedark.nvim",
    event = "VeryLazy",
    opts = {
      -- Main options --
      style = "warmer", -- Default theme style. Choose between 'dark', 'darker', 'cool', 'deep', 'warm', 'warmer' and 'light'
      transparent = false, -- Show/hide background
      term_colors = true, -- Change terminal color as per the selected theme style
      ending_tildes = true, -- Show the end-of-buffer tildes. By default they are hidden
      cmp_itemkind_reverse = false, -- reverse item kind highlights in cmp menu

      -- toggle theme style ---
      toggle_style_key = "<leader>ts", -- keybind to toggle theme style. Leave it nil to disable it, or set it to a string, for example "<leader>ts"
      toggle_style_list = { "dark", "darker", "cool", "deep", "warm", "warmer", "light" }, -- List of styles to toggle between

      -- Change code style ---
      -- Options are italic, bold, underline, none
      -- You can configure multiple style with comma separated, For e.g., keywords = 'italic,bold'
      code_style = {
        comments = "italic",
        keywords = "none",
        functions = "none",
        strings = "none",
        variables = "none",
      },

      -- Lualine options --
      lualine = {
        transparent = false, -- lualine center bar transparency
      },

      -- Custom Highlights --
      colors = {}, -- Override default colors
      highlights = {}, -- Override highlight groups

      -- Plugins Config --
      diagnostics = {
        darker = true, -- darker colors for diagnostic
        undercurl = true, -- use undercurl instead of underline for diagnostics
        background = true, -- use background color for virtual text
      },
    },
  },
  {
    "nvimdev/lspsaga.nvim",
    event = "LspAttach",
    config = function()
      require("lspsaga").setup {
        lightbulb = {
          enable = false,
          sign = false,
          debounce = 10,
          sign_priority = 40,
          virtual_text = true,
          enable_in_insert = false,
        },
      }
    end,

    dependencies = {
      "nvim-treesitter/nvim-treesitter", -- optional,
      "nvim-tree/nvim-web-devicons", -- optional
    },
  },
  {
    "sainnhe/gruvbox-material",
    event = "VeryLazy",
  },
  {
    "vimwiki/vimwiki",
    event = "VeryLazy",
    -- keys = { "<leader>ww", "<leader>wt" },
    init = function()
      vim.g.vim_markdown_folding_level = 3
      vim.g.markdown_folding = 1
      vim.g.vimwiki_list = {
        {
          path = "~/Documents/vimwiki/",
          syntax = "markdown",
          ext = ".md",
        },
      }
      vim.g.vimwiki_ext2syntax = {
        [".md"] = "markdown",
        [".markdown"] = "markdown",
        [".mdown"] = "markdown",
      }
      vim.g.vimwiki_markdown_link_ext = 1
      -- vim.api.nvim_create_autocmd("BufNewFile", {
      --   -- command = ":silent 0r !~/.vim/bin/generate-vimwiki-diary-template '%'",
      --   command = "echo 'Entering a C or C++ file'",
      --   pattern = { "~/Documents/vimwiki/diary/*.md" },
      -- })
      vim.cmd [[au BufNewFile ~/Documents/vimwiki/diary/*.md :silent 0r !~/.vim/bin/generate-vimwiki-diary-template '%'
]]
      -- au BufNewFile ~/Documents/vimwiki/diary/*.md :silent 0r !~/.vim/bin/generate-vimwiki-diary-template '%'
    end,
  },

  -- {
  --   "tools-life/taskwiki",
  --   event = "VeryLazy",
  -- },

  tag = "v0.0.7",
  -- {
  --   "ray-x/lsp_signature.nvim",
  --   event = "BufRead",
  --   config = function()
  --     require("lsp_signature").setup()
  --   end,
  -- },
}
