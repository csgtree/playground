return {
  "AstroNvim/astrocommunity",
  -- packs
  { import = "astrocommunity.pack.rust" },
  { import = "astrocommunity.pack.go" },
  { import = "astrocommunity.pack.lua" },
  { import = "astrocommunity.pack.markdown" },
  { import = "astrocommunity.pack.python" },
  { import = "astrocommunity.pack.html-css" },
  { import = "astrocommunity.pack.typescript" },

  -- tools
  {
    import = "astrocommunity.programming-language-support.rest-nvim",
    enabled = true,
  },
  -- colorscheme
  { import = "astrocommunity.colorscheme.gruvbox-nvim", enabled = true },
  { import = "astrocommunity.colorscheme.kanagawa-nvim", enabled = true },
  { -- further customize the options set by the community
    "kanagawa.nvim",
    event = "VeryLazy",
  },
  { import = "astrocommunity.colorscheme.tokyonight-nvim", enabled = true },
  { -- further customize the options set by the community
    "tokyonight.nvim",
    event = "VeryLazy",
  },
  { import = "astrocommunity.colorscheme.gruvbox-baby", enabled = true },
  { -- further customize the options set by the community
    "gruvbox-baby",
    event = "VeryLazy",
  },
  { import = "astrocommunity.colorscheme.catppuccin" },
  { -- further customize the options set by the community
    "catppuccin",
    event = "VeryLazy",
    opts = {
      integrations = {
        sandwich = false,
        noice = true,
        mini = true,
        leap = true,
        markdown = true,
        neotest = true,
        cmp = true,
        overseer = true,
        lsp_trouble = true,
        rainbow_delimiters = true,
      },
    },
  },
}
