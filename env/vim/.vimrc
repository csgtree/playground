

" filetype off
filetype off    " Required
filetype plugin on

" enable syntax
syntax enable
syntax on

" set compatibility to Vim only
set nocompatible

" leader key
let mapleader = " "

" useful for navigating
set relativenumber
set nu rnu

" backup setting
set backupdir=/tmp
set directory=~/.vim/tmp,.
set backup

" tab setting
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

" highlight search
set hlsearch

" highlight line
set cursorline

" show match count
set shortmess-=S

" stop auto commenting
set formatoptions-=cro

" split setting
set splitbelow splitright

" persistent undo between closing and opening
set undofile

" useful for yaml and python
autocmd Filetype yaml set cursorcolumn
autocmd Filetype yml set cursorcolumn
autocmd Filetype python set cursorcolumn
autocmd Filetype py set cursorcolumn

" smart search
set ignorecase
set smartcase

" smart wrapping
set wrap
set textwidth=80
set formatoptions=qrn1

" search as characters are entered
set incsearch 

">>> key bindings

" windows
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


" floaterm
let g:floaterm_keymap_new = '<Leader>ft'
let g:floaterm_keymap_toggle = '<Leader>t'
let g:floaterm_keymap_prev = '<Leader>pp'
let g:floaterm_keymap_next = '<Leader>nn'
nnoremap  <leader>fk :FloatermKill!<CR>

" turn spelling on an off
nnoremap <leader>s :setlocal spell!<CR>

" yank/paste with system clipboard
nnoremap <leader>y "+y<CR>
vnoremap <leader>y "+y<CR>
nnoremap <leader>p "+p<CR>

" toggle goyo
nnoremap <leader>g :Goyo<CR>

" escape
inoremap fd <ESC>
cnoremap fd <ESC>
nnoremap fd <ESC>
xnoremap fd <Esc>

" fzf and rg
nnoremap <C-p> :Files<Cr>
nnoremap <C-g> :Rg<Cr>
nnoremap <C-f> :BLines<Cr>
" nnoremap <C-h> :History<Cr>

" netwr
" open Netrw in the directory of the current file
" open Netrw in the current working directory
nnoremap <leader>dd :Lexplore %:p:h<CR>
nnoremap <Leader>da :Lexplore<CR>

">>> vim plug
call plug#begin()

" Plug 'ThePrimeagen/vim-be-good'
Plug 'takac/vim-hardtime'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

Plug 'junegunn/fzf.vim'

Plug 'airblade/vim-rooter'

Plug 'flazz/vim-colorschemes'

" Plug 'nathanaelkane/vim-indent-guides'
" Plug 'Yggdroot/indentLine'

Plug 'https://github.com/junegunn/goyo.vim'

Plug 'https://github.com/machakann/vim-highlightedyank'

Plug 'https://github.com/jiangmiao/auto-pairs'

"Plug 'lambdalisue/fern.vim'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

Plug 'tpope/vim-commentary'

Plug 'tpope/vim-fugitive'

"Plug 'https://github.com/vim-syntastic/syntastic.git'

Plug 'preservim/vimux'

Plug 'voldikss/vim-floaterm'

Plug 'natebosch/vim-lsc'

Plug 'vimwiki/vimwiki'

Plug 'godlygeek/tabular'

" Plug 'preservim/vim-markdown'

Plug 'mhinz/vim-startify'

Plug 'christoomey/vim-tmux-navigator'

" Plug 'tools-life/taskwiki'


call plug#end()

">>> goyo setting
let g:goyo_width = 100

">>> theme
set background=dark
"let g:solarized_termcolors = 256
"let g:solarized_visibility = "normal"
"let g:solarized_contrast = "normal"
"colorscheme solarized8
colorscheme gruvbox


">>> status bar

set laststatus=2
set statusline=
set statusline+=%#PmenuSel#
"set statusline+=%{StatuslineGit()}
set statusline +=\ %{fugitive#statusline()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 



">>> Excluding file name from vim fzf ripgrep
command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)

">>>> vim-go setting
let g:go_fmt_command = "goimports"
" let g:go_def_mode='gopls'
" let g:go_info_mode='gopls'


">>> vim lsc for golang
let g:lsc_server_commands = {
\  "go": {
\    "command": "gopls serve",
\    "log_level": -1,
\    "suppress_stderr": v:true,
\  },
\}


" ... or set only the commands you want mapped without defaults.
" Complete default mappings are:
let g:lsc_auto_map = {
    \ 'GoToDefinition': 'gd',
    \ 'GoToDefinitionSplit': 'gds',
    \ 'FindReferences': 'gr',
    \ 'NextReference': '<C-nn>',
    \ 'PreviousReference': '<C-pp>',
    \ 'FindImplementations': 'gI',
    \ 'FindCodeActions': 'ga',
    \ 'Rename': 'gR',
    \ 'ShowHover': v:true,
    \ 'DocumentSymbol': 'go',
    \ 'WorkspaceSymbol': 'gS',
    \ 'SignatureHelp': 'gm',
    \ 'Completion': 'completefunc',
    \}

">>> vim lsc for python
" let g:lsc_server_commands = { 'python' : 'pyls'}

">>> fold markdown
let g:markdown_folding = 1

">>> vim wiki
let g:vimwiki_list = [{'path': '~/Bitbucket/vimwiki',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

let g:vimwiki_markdown_link_ext = 1
" au BufNewFile ~/Bitbucket/vimwiki/diary/*.md :silent 0r !~/.vim/bin/generate-vimwiki-diary-template '%'



