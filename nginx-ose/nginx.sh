#!/bin/bash
docker run --name ose-proxy -p 8889:80 -v `pwd`/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx nginx-debug -g 'daemon off;'