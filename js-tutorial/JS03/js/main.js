// strings
const myVar = "Mathematics";

// length property
console.log(myVar.length);

// string methods
console.log(myVar.charAt(1));
console.log(myVar.indexOf("th"));
console.log(myVar.lastIndexOf("at"));
console.log(myVar.slice(5, 8));
console.log(myVar.slice(5));
console.log(myVar.slice(2, 5));
console.log(myVar.slice(5, 2));
console.log(myVar.toUpperCase());
console.log(myVar.includes("mat"));
console.log(myVar.split("e"));
console.log(myVar.split(""));
