// user input

// alert("Hello Wolrd!");

// let myBool = confirm("OK === true\nCancel === false");
// console.log(myBool);

// nullish coalescing operator
// if null or undefined, return right side operand
// else, return left side operand
//
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing
// console.log(name ?? "no name input");
let name = prompt("enter your name: ");
if (name) {
  console.log(name.trim());
} else {
  console.log("you didn't enter your name");
}
