// if statement

let customerIsBanned = true;
// let soup;
let soup = "chicken noddle soup";
let crackers = true;
let reply;

if (customerIsBanned) {
    reply = "No soup for you";
} else if (soup && crackers) {
    reply = `Here is your order of ${soup} and crakcers`;
} else if (soup) {
    reply = `Here is your order of ${soup}`;
} else {
    reply = `Sorry, out of soup`;
}
console.log(reply);

let score = 89;
let grade;

if (score >= 90) {
    grade = "A";
} else if (score >= 80) {
    grade = "B";
}

console.log(grade);
