// ternary expression

// condition ? if true : if false
// chain ternary

let soup = "fishi soup";
let isCustomerBanned = true;

let soupAccess = isCustomerBanned
	? "Sorry, no soup for you"
	: soup
	  ? `Yes, soup ${soup} for your`
	  : "no soup";

console.log(soupAccess);
