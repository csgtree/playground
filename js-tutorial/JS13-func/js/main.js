// functions

// built-in
"wangc".toLowerCase();
Math.random();

// function declaration syntax
function sum(n1, n2) {
	if (n2 === undefined) {
		return n1 + n1;
	}
	return n1 + n2;
}
console.log(sum(1, 5));

// func syntax 1
// function getUsernameFromEmail(email) {
// 	return email.slice(0, email.indexOf("@"));
// }

// func syntax 2
// const getUsernameFromEmail = function (email) {
// 	return email.slice(0, email.indexOf("@"));
// };

// func syntax 3
const getUsernameFromEmail = (email) => {
	return email.slice(0, email.indexOf("@"));
};
console.log(getUsernameFromEmail("wachen@vmware.com"));

const toProperCase = (name) => {
	return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
};
console.log(toProperCase("wachen"));
