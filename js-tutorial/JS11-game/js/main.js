let playGame = confirm("Play the game?");
let rock = "rock";
let paper = "paper";
let scissors = "scissors";

if (playGame) {
  let playerChoice = prompt(`Please enter ${rock}, ${paper} or ${scissors}`);

  if (playerChoice) {
    playerChoice = playerChoice.trim().toLowerCase();

    // validate user input
    if (
      playerChoice === rock ||
      playerChoice === paper ||
      playerChoice === scissors
    ) {
      let random = Math.floor(Math.random() * 3);
      let computerChoice =
        random === 0 ? rock : random === 1 ? paper : scissors;

      let result =
        playerChoice === computerChoice
          ? "Tie game"
          : playerChoice === rock && computerChoice === scissors
            ? `Player wins! Player ${playerChoice} and Computer ${computerChoice}`
            : playerChoice === paper && computerChoice === rock
              ? `Player wins! Player ${playerChoice} and Computer ${computerChoice}`
              : playerChoice === scissors && computerChoice === paper
                ? `Player wins! Player ${playerChoice} and Computer ${computerChoice}`
                : `Computer wins! Player ${playerChoice} and Computer ${computerChoice}`;

      alert(result);

      let again = confirm("Play again?");
      again ? location.reload() : alert("See you");
    } else {
      alert("Invalid input");
    }
  } else {
    alert("Sorry that you change your mind");
  }
} else {
  alert("Let's play next time");
}
