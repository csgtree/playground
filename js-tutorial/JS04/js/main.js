// numbers
const myNum = 56;

// an integer is a whole number
console.log(myNum);

// floating point
const myFloat = 56.220485;
console.log(myFloat);

const myString = "56.22567";
console.log(myString); // white in console

// converting
console.log(myNum === myFloat);
console.log(myNum === myString);
console.log(3 + myString);

console.log(3 + Number(myString));
console.log(myNum === Number(myString)); // string to number
console.log(Number("Foobar")); // not a number
console.log(Number(undefined)); // not a number
console.log(Number(true)); // true => 1, false =>0

// number methods
console.log(Number.isInteger(myString));
console.log(Number.parseFloat(myString));
console.log(Number.parseFloat("foobar"));
console.log(Number.parseFloat(myString).toFixed(2)); // output string instead of number
console.log(Number.parseInt(myFloat)); // output string instead of number
console.log(myFloat.toString());
console.log(typeof myFloat.toString());
console.log(typeof Number.parseFloat(myString));
console.log(Number.parseFloat("1.23").toFixed(2)); // chain

// NaN is an acronym for Not a Number
console.log(Number("foobar"));
console.log(Number.isNaN("foobar")); // confusing, type is number AND is NaN
console.log(typeof NaN); // NaN is of type number
console.log(isNaN("foobar")); // is NaN or not
