// var let and const

// var is function() scoped
// let and const {block} scoped

// tips
// avoid using var and stick with let and const
// use const unless you need to reassign value
// use let if you know you will reassign value

var x = 1;
let y = 2;
const z = 3;

console.log(`global: ${x}`);
console.log(`global: ${y}`);
console.log(`global: ${z}`);

function myFunc() {
	var x = 11;
	const z = 33;

	{
		var x = 111; // function scope
		const z = 333; // block scope
		console.log(`block: ${x}`);
		console.log(`block: ${y}`);
		console.log(`block: ${z}`);

		var foo = 1111;
		let bar = 2222;
	}

	console.log(`function: ${x}`); // 111
	console.log(`function: ${y}`);
	console.log(`function: ${z}`); // 33

	console.log(foo);
	// console.log(bar); // let is block scope so it cannot be accessed here
}

myFunc();
