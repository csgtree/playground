// var let and const

// var is legacy; not safe
var x = 1;
var x = 2;
console.log(x);

let y = 1;
// let y = 2; // error for re-declare
y = 2;

const z = 1;
// z = 2; // runtime error

// 1. prefer to use const
// 2. then let if need to assign new value
// 3. use var for legacy

// global scope, accessible everythere
var xx = 1;
let yy = 1;
const zz = 1;

// local scope

//block scope
{
	let yy = 2;
}

// func scope
function myFund() {
	const zz = 3;
	const zzz = 5;

	{
		// local scope
		let y = 5;
	}
}

console.log(zzz); // not available in global scope
