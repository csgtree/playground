// loop

let num = 50;
while (num > 0) {
  console.log(num);
  // num--;
  num -= 1;
}

num = 50;
do {
  console.log(num);
} while (num < 50);

for (let i = 0; i < 10; i++) {
  console.log(i);
}
