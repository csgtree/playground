//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math
// math methods and properties

console.log(Math.PI);
console.log(Math.trunc(Math.PI));
console.log(Math.round(3.3));
console.log(Math.round(3.5)); // not same as trunc
console.log(Math.ceil(3.3));
console.log(Math.floor(3.6));
console.log(Math.pow(2, 10));
console.log(Math.min(2, 22, 10));
console.log(Math.min(2, 0.9, 10));
console.log(Math.max(2, 0.9, 10));

console.log(Math.random()); // [0,1)
console.log(Math.random()); // [0,1)

console.log(Math.floor(10 * Math.random()) + 1); // from 1 to 10
console.log(Math.floor(10 * Math.random()) + 1); // from 1 to 10
console.log(Math.ceil(10 * Math.random())); // not work from 1 to 10 as random could be zero
