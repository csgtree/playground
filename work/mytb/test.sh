#!/bin/bash

# Function to show help message
show_help() {
    echo "Usage: $0 {start|stop|status} -f <file_path>"
    echo "  start   Start the service using the specified file"
    echo "  stop    Stop the service using the specified file"
    echo "  status  Check the service status using the specified file"
    echo "  -f      Specify the file path"
}

# Check if at least one subcommand is provided
# if [ $# -eq 0 ]; then
#     show_help
#     exit 1
# fi

# Initialize variables
file_path=""

# Parse flags
while getopts "f:" flag; do
    echo 'in do'
    case "$flag" in
        f)
            file_path=$OPTARG
            ;;
        *)
            show_help
            exit 1
            ;;
    esac
done

# Check if the file path is provided
if [ -z "$file_path" ]; then
    echo "Error: You must specify a file with -f"
    show_help
    exit 1
fi

# Shift to process the subcommand after the flags
shift $((OPTIND - 1))

# Check if no subcommand is provided
if [ $# -eq 0 ]; then
    show_help
    exit 1
fi

# Handle subcommands
case "$1" in
    start)
        echo "Starting the service using file: $file_path"
        # Add logic to start service using $file_path
        # For example, you could check if the file exists:
        if [ -f "$file_path" ]; then
            echo "Service started using file: $file_path"
            # Actual start logic here
        else
            echo "Error: File does not exist: $file_path"
            exit 1
        fi
        ;;
    stop)
        echo "Stopping the service using file: $file_path"
        # Add logic to stop service using $file_path
        if [ -f "$file_path" ]; then
            echo "Service stopped using file: $file_path"
            # Actual stop logic here
        else
            echo "Error: File does not exist: $file_path"
            exit 1
        fi
        ;;
    status)
        echo "Checking the service status using file: $file_path"
        # Add logic to check service status using $file_path
        if [ -f "$file_path" ]; then
            echo "Service status checked using file: $file_path"
            # Actual status check logic here
        else
            echo "Error: File does not exist: $file_path"
            exit 1
        fi
        ;;
    *)
        echo "Invalid subcommand: $1"
        show_help
        exit 1
        ;;
esac
