#!/bin/bash


# name=wangc
name=dsm-fqdn
# name=dsm-no-fqdn
# name=vcfa-fqdn
# name=vcfa-no-fqdn
kubectl --kubeconfig ./kubernetes-service-kubeconfig.yaml get secret $name-kubeconfig -n minio-default -o jsonpath={.data.value} | base64 -d > kubeconfig-objectstore-wl.yaml
export KUBECONFIG=./kubeconfig-objectstore-wl.yaml

