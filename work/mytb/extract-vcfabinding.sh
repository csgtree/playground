kubectl get -n dsm-system cm vcfa-ca -oyaml > vcfa-ca.yaml
kubectl get -n dsm-system secrets vcfa-serviceaccount -oyaml > vcfa-sa.yaml
kubectl get -n dsm-system vcfabindings.system.dataservices.vmware.com vcfa -oyaml > vcfabinding.yaml

cat vcfa-ca.yaml > vcfa.yaml
echo "---" >> vcfa.yaml
cat vcfa-sa.yaml >> vcfa.yaml
echo "---" >> vcfa.yaml
cat vcfabinding.yaml >> vcfa.yaml
