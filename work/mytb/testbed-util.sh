#/bin/bash

# Function to show help message
show_help() {
    echo "Usage: $0 {jm|dm|proxy} [-f]"
    echo "  jm  Push SSH key to jump machine"
    echo "  dm  Push SSH key to DSM machine"
    echo "  proxy  Start the proxy"
    echo "  -f  Specify the path to dsm_spec.json. Defaults to ./dsm_spec.json"
}

setup_jump_machine(){
    echo 'NIMBUS Network'

    echo '  Jump machine IP' 
    echo '    '$1

    echo '  Jump machine User' 
    echo '    '$2

    echo '  Jump machine Password'
    echo '    '$3


    sshpass -p $3 ssh-copy-id $2@$1
}

setup_dsm_machine(){
    echo 'DSM VM'

    echo '  DSM IP' 
    echo '    '$1

    echo '  DSM SSH User' 
    echo '    '$2

    echo '  DSM SSH Password'
    echo '    '$3


    sshpass -p $3 ssh-copy-id $2@$1
}

start_proxy(){
    sshuttle -r "$1"@"$2" 192.168.111.0/24 --no-latency-control
}


# At least one subcommand
if [ $# -lt 1 ]; then
    show_help
    exit 1
fi

# Shift after extract subcommand to process flag
subcommand=$1
shift

# Initialize variables
file_path="./dsm_spec.json"

# Parse flags
while getopts "f:" flag; do
    case "$flag" in
        f)
            file_path="$OPTARG"
            ;;
        *)
            show_help
            exit 1
            ;;
    esac
done

# Check if the file path is provided
if [ -z "$file_path" ]; then
    echo "Error: You must specify dsm_spec file with -f"
    show_help
    exit 1
fi


jump_ip=`cat $file_path | jq -r '.nimbusInfo.network.[0].gateway'`
jump_username=`cat $file_path| jq -r '.nimbusInfo.network.[0].username'`
jump_password=`cat $file_path | jq -r '.nimbusInfo.network.[0].password'`

dsm_ip=`cat $file_path | jq -r '.providerSpec.[0].ip'`
dsm_username=`cat $file_path| jq -r '.providerSpec.[0].sshUser'`
dsm_password=`cat $file_path | jq -r '.providerSpec.[0].sshPassword'`


# Handle subcommands
case "$subcommand" in
    jm)
        echo "Push SSH key to jump machine..."
        setup_jump_machine $jump_ip $jump_username $jump_password
        ;;
    dm)
        echo "Push SSH key to DSM machine..."
        setup_dsm_machine $dsm_ip $dsm_username $dsm_password
        ;;
    proxy)
        echo "Start proxy tunnel on jump machine..."
        start_proxy $jump_username $jump_ip
        ;;
    *)
        echo "Invalid subcommand: $subcommand"
        show_help
        exit 1
        ;;
esac
