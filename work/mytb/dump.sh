#/bin/bash
echo 'DSM VM'
echo '  IP' 
echo '    '`cat dsm_spec.json | jq -r '.providerSpec.[0].ip'`
echo '  SSH User' 
echo '    '`cat dsm_spec.json | jq -r '.providerSpec.[0].sshUser'`
echo '  SSH Password'
echo '    '`cat dsm_spec.json | jq -r '.providerSpec.[0].sshPassword'`
