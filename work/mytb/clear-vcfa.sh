#!/bin/bash

kc -n dsm-system delete secrets vcfa-serviceaccount
kc -n dsm-system delete configmaps vcfa-ca
kc -n dsm-system delete vcfabindings.system.dataservices.vmware.com vcfa
