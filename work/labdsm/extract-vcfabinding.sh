kubectl get -n dsm-system cm vcfa-ca -oyaml > vcfa-ca.yaml
kubectl get -n dsm-system secrets vcfa-serviceaccount -oyaml > vcfa-sa.yaml
kubectl get -n dsm-system vcfabindings.system.dataservices.vmware.com vcfa -oyaml > vcfabinding.yaml

cat vcfa-ca.yaml > cr-vcfa.yaml
echo "---" >> cr-vcfa.yaml
cat vcfa-sa.yaml >> cr-vcfa.yaml
echo "---" >> cr-vcfa.yaml
cat vcfabinding.yaml >> cr-vcfa.yaml
