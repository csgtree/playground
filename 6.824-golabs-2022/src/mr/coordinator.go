package mr

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type MapTask struct {
	TaskId    string
	Input     string
	Outputs   []string
	Status    TaskStatus
	IssueTime int64
}

type ReduceTask struct {
	TaskId    string
	Inputs    []string
	Output    string
	Status    TaskStatus
	IssueTime int64
}

type Coordinator struct {
	// Your definitions here.
	mapTaskMap    map[string]*MapTask
	reduceTaskMap map[string]*ReduceTask
	nReduce       int

	cLock sync.RWMutex
}

func (rt ReduceTask) String() string {
	return fmt.Sprintf("TaskId: %s, Status: %v, Inputs: %s, Output: %s\n", rt.TaskId, rt.Status, strings.Join(rt.Inputs, "|"), rt.Output)
}

func (mt MapTask) String() string {
	return fmt.Sprintf("TaskId: %s, Status: %v, Input: %s, Outputs: %s\n", mt.TaskId, mt.Status, mt.Input, strings.Join(mt.Outputs, "|"))
}

// Your code here -- RPC handlers for the worker to call.

//
// an example RPC handler.
//
// the RPC argument and reply types are defined in rpc.go.
//
func (c *Coordinator) Example(args *ExampleArgs, reply *ExampleReply) error { // handle the specific rpc call
	reply.Y = args.X + 1
	return nil
}

func (c *Coordinator) GetTask(args *TaskArgs, reply *TaskReply) error {

	log.Println("Enter RPC get task")

	c.lock()

	if c.allMapTasksDone() && c.allRecudeTasksDone() {
		reply.Type = TypeShutdown
		log.Println("Coordinator issues shuts down command.")
	} else if c.allMapTasksDone() && !c.allRecudeTasksDone() {

		log.Println("All map tasks are completed")
		log.Printf("Reduce tasks are %++v\n", c.reduceTaskMap)

		rTask := c.fetchReduceTask()
		log.Printf("Return reduce task %+v\n", rTask)
		if rTask != nil {
			reply.Type = TypeReduce
			reply.Inputs = rTask.Inputs
			reply.TaskId = rTask.TaskId
		} else {
			reply.Type = TypeIdle
		}

	} else {
		mTask := c.fetchMapTask()
		log.Printf("Return map task %+v\n", mTask)

		if mTask != nil {
			reply.Type = TypeMap
			reply.Inputs = []string{mTask.Input}
			reply.TaskId = mTask.TaskId
			reply.NReduce = c.nReduce
		} else {
			reply.Type = TypeIdle
		}
	}

	log.Printf("Exit RPC get task %+v\n", reply)

	c.unlock()

	return nil
}
func (c *Coordinator) CommitTask(args *CommitArgs, reply *CommitReply) error {

	log.Printf("Enter RPC commit task with args %+v\n", args)

	c.lock()

	//TODO multi commit
	if args.Type == TypeMap && args.Status == StatusDone && len(c.mapTaskMap) > 0 {

		_, ok := c.mapTaskMap[args.TaskId]
		if !ok {
			log.Printf("Map task TaskId=%s and Outputs=%v is already committed", args.TaskId, args.Outputs)
			return nil
		}

		log.Printf("Ready to remove map task TaskId=%s from collection %+v\n", args.TaskId, c.mapTaskMap)
		delete(c.mapTaskMap, args.TaskId)
		log.Printf("Complete to remove map task TaskId=%s from collection %+v\n", args.TaskId, c.mapTaskMap)

		for _, output := range args.Outputs {
			log.Println("Map task output is %s\n", output)
			parts := strings.Split(output, "-")

			reduceTaskId := parts[2]

			// get reduce task (create one if not exist)
			rTask, ok := c.reduceTaskMap[reduceTaskId]
			if !ok {
				rTask = &ReduceTask{
					TaskId: reduceTaskId,
					Inputs: []string{},
					Output: "",
					Status: StatusReady,
				}
				log.Printf("Reduce task to add is %+v\n", *rTask)
				c.reduceTaskMap[reduceTaskId] = rTask
				log.Printf("Add reduce task; reduceTaskMap is %+v\n", c.reduceTaskMap)
			}

			rTask.Inputs = append(rTask.Inputs, output)

		}

		log.Printf("After map task committed, reduceTaskMap is %+v\n", c.reduceTaskMap)
	} else if args.Type == TypeReduce && args.Status == StatusDone && len(c.reduceTaskMap) > 0 {
		_, ok := c.reduceTaskMap[args.TaskId]
		if !ok {
			log.Printf("Reduce task TaskId=%s and Outputs=%v is already committed", args.TaskId, args.Outputs)
			return nil
		}

		log.Printf("Ready to remove reduce task TaskId=%s from collection %+v\n", args.TaskId, c.reduceTaskMap)
		delete(c.reduceTaskMap, args.TaskId)
		log.Printf("Complete to remove reduce task TaskId=%s from collection %+v\n", args.TaskId, c.reduceTaskMap)
	}

	c.unlock()

	log.Printf("Exit RPC commit task\n")

	return nil
}

func (c *Coordinator) fetchMapTask() *MapTask {

	log.Printf("Enter fetchMapTask. mapTasks is  %+v\n", c.mapTaskMap)
	var ret *MapTask = nil
	for _, v := range c.mapTaskMap {
		if v.Status == StatusRunning && (time.Now().Unix()-v.IssueTime) > 10 {
			v.IssueTime = time.Now().Unix()
			ret = v
			log.Println("Re-issue a map task.")
			break
		}
		if v.Status == StatusReady {
			v.Status = StatusRunning
			v.IssueTime = time.Now().Unix()
			ret = v
			log.Println("Issue a map task.")
			break
		}
	}

	log.Printf("Fetch map task in ready status %+v\n", ret)
	log.Printf("Exit fetchMapTask. mapTasks is  %+v\n", c.mapTaskMap)
	return ret
}

func (c *Coordinator) fetchReduceTask() *ReduceTask {

	log.Printf("Enter fetchReduceTask. reduceTasks is  %+v\n", c.reduceTaskMap)
	var ret *ReduceTask = nil
	for _, v := range c.reduceTaskMap {
		if v.Status == StatusRunning && (time.Now().Unix()-v.IssueTime) > 10 {
			v.IssueTime = time.Now().Unix()
			ret = v
			log.Println("Re-issue a reduce task.")
			break
		} else if v.Status == StatusReady {
			v.Status = StatusRunning
			v.IssueTime = time.Now().Unix()
			ret = v
			log.Println("Issue a reduce task.")
			break
		}
	}

	log.Printf("Fetch reduce task in ready status %+v\n", ret)
	log.Printf("Exit fetchReduceTask. reduceTasks is  %+v\n", c.reduceTaskMap)
	return ret
}

func (c *Coordinator) allMapTasksDone() bool {

	done := true
	for _, v := range c.mapTaskMap {
		if v.Status != StatusDone {
			done = false
		}
	}

	return done
}

func (c *Coordinator) allRecudeTasksDone() bool {
	done := true
	for _, v := range c.reduceTaskMap {
		if v.Status != StatusDone {
			done = false
		}
	}

	return done
}

//
// start a thread that listens for RPCs from worker.go
//
func (c *Coordinator) server() { //reciever func
	rpc.Register(c)
	rpc.HandleHTTP()
	//l, e := net.Listen("tcp", ":1234")
	sockname := coordinatorSock()
	os.Remove(sockname) // remove the socket if existed
	l, e := net.Listen("unix", sockname)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
}

//
// main/mrcoordinator.go calls Done() periodically to find out
// if the entire job has finished.
//
func (c *Coordinator) Done() bool {

	// Your code here.
	time.Sleep(5 * time.Second)
	c.rLock()
	done := c.allMapTasksDone() && c.allRecudeTasksDone()
	c.rUnlock()
	return done

}

func (c *Coordinator) rLock() {
	log.Println("Read lock")
	c.cLock.RLock()
}
func (c *Coordinator) rUnlock() {
	log.Println("Read unlock")
	c.cLock.RUnlock()
}

func (c *Coordinator) lock() {
	log.Println("Lock")
	c.cLock.Lock()
}

func (c *Coordinator) unlock() {
	log.Println("Unlock")
	c.cLock.Unlock()
}

//
// create a Coordinator.
// main/mrcoordinator.go calls this function.
// nReduce is the number of reduce tasks to use.
//
func MakeCoordinator(files []string, nReduce int) *Coordinator {
	logFile, err := os.OpenFile("./coordinator.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(logFile)
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.Lmicroseconds)

	c := Coordinator{}

	// Your code here.

	// dump inputs of coordinator
	log.Printf("Number of reduce tasks is %d\nInput files to process are: %v\n", nReduce, files)

	c.lock()
	// init map tasks
	c.mapTaskMap = make(map[string]*MapTask)
	for i, file := range files {
		c.mapTaskMap[strconv.Itoa(i)] = &MapTask{
			TaskId:  strconv.Itoa(i),
			Input:   file,
			Outputs: []string{},
			Status:  StatusReady,
		}
	}

	log.Printf("Map tasks are initialized: %+v\n", c.mapTaskMap)

	// init reduce number
	c.nReduce = nReduce

	// init reduce task map
	c.reduceTaskMap = make(map[string]*ReduceTask)

	c.unlock()

	// serve rpc
	c.server()
	return &c
}
