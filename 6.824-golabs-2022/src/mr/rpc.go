package mr

//
// RPC definitions.
//
// remember to capitalize all names.
//

import (
	"os"
	"strconv"
)

//
// example to show how to declare the arguments
// and reply for an RPC.
//

type ExampleArgs struct {
	X int
}

type ExampleReply struct {
	Y int
}

// task model
type TaskArgs struct {
}

type TaskType int

const (
	TypeMap TaskType = iota
	TypeReduce
	TypeShutdown
	TypeIdle
)

type TaskStatus int

const (
	StatusReady TaskStatus = iota
	StatusRunning
	StatusDone
	StatusHung
	StatusError
)

type TaskReply struct {
	Type    TaskType
	Inputs  []string
	TaskId  string
	NReduce int
}

// commit model
type CommitArgs struct {
	Type    TaskType
	TaskId  string
	Outputs []string
	Status  TaskStatus
}

type CommitReply struct {
}

// Add your RPC definitions here.

// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the coordinator.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func coordinatorSock() string {
	s := "/var/tmp/824-mr-"
	s += strconv.Itoa(os.Getuid()) // generate a stable socket name
	return s
}
