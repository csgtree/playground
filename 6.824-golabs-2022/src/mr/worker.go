package mr

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"log"
	"net/rpc"
	"os"
	"plugin"
	"sort"
	"strconv"
	"time"
)

//
// Map functions return a slice of KeyValue.
//
type KeyValue struct {
	Key   string
	Value string
}

// for sorting by key.
type ByKey []KeyValue

// for sorting by key.
func (a ByKey) Len() int           { return len(a) }
func (a ByKey) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByKey) Less(i, j int) bool { return a[i].Key < a[j].Key }

type Task struct {
	Type         TaskType
	TaskId       string
	Inputs       []string
	Outputs      []string
	ReducerCount int
}

type KvBucket []KeyValue

//
// use ihash(key) % NReduce to choose the reduce
// task number for each KeyValue emitted by Map.
//
func ihash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))               // use 32 bits hash to process key
	return int(h.Sum32() & 0x7fffffff) // calculate mod
}

//
// main/mrworker.go calls this function.
//
func Worker(mapf func(string, string) []KeyValue,
	reducef func(string, []string) string) {

	logFile, err := os.OpenFile("./worker.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(logFile)
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.Lmicroseconds)

	// Your worker implementation here.
	task := GetTask()
	for task != nil && task.Type != TypeShutdown {

		log.Printf("Get task %+v\n", task)

		if task.Type == TypeIdle {
			time.Sleep(2 * time.Second)
		} else {
			if task.Type == TypeMap {
				Map(mapf, task)
			}

			if task.Type == TypeReduce {
				Reduce(reducef, task)
			}

			CommitTask(*task)
		}

		task = GetTask()
	}
	log.Println("Worker exits gracefully.")
	os.Exit(0)

	// uncomment to send the Example RPC to the coordinator.
	// CallExample()

}

func Map(mapf func(string, string) []KeyValue, task *Task) {
	log.Printf("Enter Map function with input %s\n", task.Inputs)
	inputFilename := task.Inputs[0]

	// open file and read content
	file, err := os.Open(inputFilename)
	if err != nil {
		log.Fatalf("cannot open %v", inputFilename) // Fatalf is equivalent to Printf() followed by a call to os.Exit(1).
	}

	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("cannot read %v", inputFilename) // %v表示default format

	}
	file.Close() // 读完就close文件

	// call map function to process input file
	intermediate := mapf(inputFilename, string(content)) // TODO mapf负责read文件？
	// intermediate = append(intermediate, kva...)          // 类似Java语法，合并slice
	sort.Sort(ByKey(intermediate)) // ByKey实现了reciever函数Len/Less/Swap

	// distribute the kv to reduce tasks
	// The map phase should divide the intermediate keys into buckets for nReduce reduce tasks, where nReduce is the number of reduce tasks -- argument that main/mrcoordinator.go passes to MakeCoordinator(). So, each mapper needs to create nReduce intermediate files for consumption by the reduce tasks.

	kvBuckets := make([]KvBucket, task.ReducerCount)
	for i, _ := range kvBuckets {
		kvBuckets[i] = []KeyValue{} // TODO is this necessary to init the slice
	}

	log.Printf("Intermediate kv for file %s is of length %d\n", inputFilename, len(intermediate))
	time.Sleep(2 * time.Second)

	i := 0
	for i < len(intermediate) {
		j := i + 1
		for j < len(intermediate) && intermediate[j].Key == intermediate[i].Key {
			j++
		} // [i,j)是Key相同的序列

		reduceNum := ihash(intermediate[i].Key) % task.ReducerCount

		for k := i; k < j; k++ {
			kvBuckets[reduceNum] = append(kvBuckets[reduceNum], intermediate[k])
		}

		i = j
	}

	log.Printf("There are %d buckets in all\n", len(kvBuckets))
	outputs := []string{}
	for i, bucket := range kvBuckets {
		oFilename := fmt.Sprintf("mr-%s-%d", task.TaskId, i)

		// tempFile, _ := os.Create(oFilename)
		tempFile, _ := ioutil.TempFile("", strconv.FormatInt(time.Now().UnixNano(), 10))
		enc := json.NewEncoder(tempFile)

		for _, kv := range bucket {
			err := enc.Encode(&kv)

			if err != nil {
				log.Fatalf("cannot write to file ", tempFile.Name())
			}
		}

		log.Printf("Write %d KV to file %s", len(bucket), tempFile.Name())

		tempFile.Close()

		os.Rename(tempFile.Name(), oFilename)
		outputs = append(outputs, oFilename)
	}
	task.Outputs = outputs

}
func Reduce(reducef func(string, []string) string, task *Task) {

	log.Printf("Enter Reduce function wit input %s\n", task.Inputs)
	oFilename := fmt.Sprintf("mr-out-%s", task.TaskId)
	// tempFile, _ := os.Create(oname)
	tempFile, _ := ioutil.TempFile("", strconv.FormatInt(time.Now().UnixNano(), 10))

	intermediateKv := []KeyValue{}
	for _, input := range task.Inputs {
		// read from intermediate file

		// load file
		intermediateFile, err := os.Open(input)
		if err != nil {
			log.Fatalf("cannot open %v", intermediateFile)
		}

		dec := json.NewDecoder(intermediateFile)
		for {
			var kv KeyValue
			if err := dec.Decode(&kv); err != nil {
				break
			}
			intermediateKv = append(intermediateKv, kv)
		}

		intermediateFile.Close()
	}
	// reduce
	//
	// call Reduce on each distinct key in intermediate[],
	// and print the result to mr-out-0.
	//
	sort.Sort(ByKey(intermediateKv))
	i := 0
	for i < len(intermediateKv) {
		j := i + 1
		for j < len(intermediateKv) && intermediateKv[j].Key == intermediateKv[i].Key {
			j++
		} // [i,j)是Key相同的序列
		values := []string{}
		for k := i; k < j; k++ {
			values = append(values, intermediateKv[k].Value)
		}
		output := reducef(intermediateKv[i].Key, values)

		// this is the correct format for each line of Reduce output.
		fmt.Fprintf(tempFile, "%v %v\n", intermediateKv[i].Key, output) // 输出到文件

		i = j
	}

	tempFile.Close()
	os.Rename(tempFile.Name(), oFilename)
}

func GetTask() (task *Task) {
	args := TaskArgs{}
	reply := TaskReply{}

	log.Println("Call RPC getTask")

	ok := call("Coordinator.GetTask", &args, &reply)
	if ok {
		log.Printf("RPC getTask reply: %v\n", reply)
		return &Task{
			Type:         reply.Type,
			Inputs:       reply.Inputs,
			TaskId:       reply.TaskId,
			ReducerCount: reply.NReduce,
		}
	} else {
		log.Println("RPC getTask failed!")
		return nil
	}
}

func CommitTask(task Task) {
	args := CommitArgs{
		Type:    task.Type,
		TaskId:  task.TaskId,
		Status:  StatusDone,
		Outputs: task.Outputs,
	}
	reply := CommitReply{}

	log.Println("Call RPC commitTask")
	ok := call("Coordinator.CommitTask", &args, &reply)
	if ok {
		log.Println("RPC commitTask succeeds")
	} else {
		log.Println("RPC commitTask failed!")
	}
}

//
// example function to show how to make an RPC call to the coordinator.
//
// the RPC argument and reply types are defined in rpc.go.
//
func CallExample() {

	// declare an argument structure.
	args := ExampleArgs{}

	// fill in the argument(s).
	args.X = 99

	// declare a reply structure.
	reply := ExampleReply{}

	// send the RPC request, wait for the reply.
	// the "Coordinator.Example" tells the
	// receiving server that we'd like to call
	// the Example() method of struct Coordinator.
	ok := call("Coordinator.Example", &args, &reply)
	if ok {
		// reply.Y should be 100.
		log.Printf("reply.Y %v\n", reply.Y)
	} else {
		log.Printf("call failed!\n")
	}
}

//
// send an RPC request to the coordinator, wait for the response.
// usually returns true.
// returns false if something goes wrong.
//
func call(rpcname string, args interface{}, reply interface{}) bool {
	// c, err := rpc.DialHTTP("tcp", "127.0.0.1"+":1234")
	sockname := coordinatorSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	log.Println(err)
	return false
}

//
// load the application Map and Reduce functions
// from a plugin file, e.g. ../mrapps/wc.so
//
func loadPlugin(filename string) (func(string, string) []KeyValue, func(string, []string) string) {
	p, err := plugin.Open(filename)
	if err != nil {
		log.Fatalf("cannot load plugin %v", filename)
	}
	xmapf, err := p.Lookup("Map")
	if err != nil {
		log.Fatalf("cannot find Map in %v", filename)
	}
	mapf := xmapf.(func(string, string) []KeyValue)
	xreducef, err := p.Lookup("Reduce")
	if err != nil {
		log.Fatalf("cannot find Reduce in %v", filename)
	}
	reducef := xreducef.(func(string, []string) string)

	return mapf, reducef
}
