-- Print
print("Hello World!")

-- Variable
local hi = "Hello!"
local n = 55
print(hi)
print(n)

-- functions
local function handy()
	print("Handy parter!")
end

handy()
handy()
handy()

-- Math Equations
local a = 1
local b = 2
local answer1 = a + b
print(answer1)
local answer2 = a - b
print(answer2)
local answer3 = a * b
print(answer3)
local answer4 = a / b
print(answer4)
local answer5 = a ^ b
print(answer5)
print(a % b)

-- Text Input
print("What's your name?\n")
-- local name = io.read()
-- print("Hi " .. name)

-- If Statement
local first = 6
local second = 6

if first == second then
	-- < less
	-- > greater
	-- <=less or equal
	-- >= greater or equal
	-- ~= inequal
	print("Correct")
else
	print("Not Correct")
end

-- Challenge
local price = 20.00
local taxrate = 0.06

local tax = price * taxrate
local total = price + tax

print("Starting price " .. price)
print("Price with tax " .. total)
print("Tax " .. tax)
print("Tax rate " .. taxrate)
