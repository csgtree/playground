package utils

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
)

func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func ParsePaginationParams(r *http.Request) (int, int) {
	values := r.URL.Query()
	limit, _ := strconv.Atoi(values.Get("limit"))
	offset, _ := strconv.Atoi(values.Get("offset"))

	return offset, limit
}

func ParseBody(r *http.Request, x interface{}) {
	if body, err := ioutil.ReadAll(r.Body); err == nil {
		if err := json.Unmarshal([]byte(body), x); err != nil {
			return
		}
	}
}
