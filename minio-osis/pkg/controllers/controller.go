package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"osis.vmware.com/minio/pkg/models"
	"osis.vmware.com/minio/pkg/services"
	"osis.vmware.com/minio/pkg/utils"
)

func PatchTenant(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotImplemented)
}

func HeadTenant(w http.ResponseWriter, r *http.Request) {

	tId := mux.Vars(r)["tenantId"]

	if services.HeadTenant(tId) {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func GetTenants(w http.ResponseWriter, r *http.Request) {

	offset, limit := utils.ParsePaginationParams(r)

	w.WriteHeader(http.StatusOK)
	response, _ := json.Marshal(services.GetTenants(offset, limit))
	w.Write(response)
}

func CreateTenant(w http.ResponseWriter, r *http.Request) {
	t := &models.Tenant{}
	utils.ParseBody(r, t)
	response, _ := json.Marshal(services.CreateTenant(*t))
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}
