package services

import (
	"context"
	"log"

	"github.com/minio/madmin-go"
)

func getInfo() {
	madmClnt, err := madmin.New("10.110.165.191:32000", "minio", "minioadmin", false)
	if err != nil {
		log.Fatalln(err)
	}

	infoMsg, err := madmClnt.ServerInfo(context.Background())
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", infoMsg.Mode)
	log.Printf("%+v\n", infoMsg.Buckets)
}
