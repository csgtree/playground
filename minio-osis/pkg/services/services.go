package services

import (
	"context"
	"log"
	"sort"
	"strings"

	"github.com/minio/madmin-go"
	"osis.vmware.com/minio/pkg/config"
	"osis.vmware.com/minio/pkg/models"
	"osis.vmware.com/minio/pkg/utils"
)

const DOUBLE_COLON = "::"
const SINGLE_COLON = ":"
const ENABLED = "enabled"
const DISABLED = "disabled"

var adminClient *madmin.AdminClient

func init() {
	config.Init()
	adminClient = config.GetAdminClient()
}

func GetInfo() {
	infoMsg, err := adminClient.ServerInfo(context.Background())
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", infoMsg.Buckets)
}

func QueryTenants(filter string, offset int, limit int) {

}

func HeadTenant(tId string) bool {
	t := GetTenant(tId)

	return t != nil
}

func GetTenant(tId string) *models.Tenant {

	tenants := getTenants()

	for _, t := range tenants {
		if t.TenantId == tId {
			return &t
		}
	}

	return nil
}

func CreateTenant(t models.Tenant) *models.Tenant {

	cdTidsSeg := strings.Join(t.CdTenantIds, SINGLE_COLON)
	gar := madmin.GroupAddRemove{
		Group:    strings.Join([]string{t.Name, t.TenantId, cdTidsSeg}, DOUBLE_COLON),
		Members:  []string{},
		Status:   "",
		IsRemove: false,
	}
	adminClient.UpdateGroupMembers(context.Background(), gar)
	return &t
}

func GetTenants(offset int, limit int) []models.Tenant {

	tenants := getTenants()
	start := utils.Min(offset, len(tenants))
	end := utils.Min(offset+limit, len(tenants))

	return tenants[start:end]
}

func getTenants() []models.Tenant {

	groups, err := adminClient.ListGroups(context.Background())
	if err != nil {
		log.Fatalln(err)
	}
	sort.Strings(groups)

	tenants := []models.Tenant{}
	for _, g := range groups {

		gDes, err := adminClient.GetGroupDescription(context.Background(), g)
		if err != nil {
			log.Fatalln(err)
		}

		if !strings.Contains(g, DOUBLE_COLON) {
			continue
		}

		gNameAndIdAndCid := strings.Split(g, DOUBLE_COLON)[0:3]

		tenants = append(tenants, models.Tenant{
			Name:        gNameAndIdAndCid[0],
			Active:      (gDes.Status == ENABLED),
			TenantId:    gNameAndIdAndCid[1],
			CdTenantIds: strings.Split(gNameAndIdAndCid[2], SINGLE_COLON),
		})
	}

	return tenants
}
