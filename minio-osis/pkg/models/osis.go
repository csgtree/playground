package models

type Tenant struct {
	Name        string   `json:"name"`
	Active      bool     `json:"active"`
	TenantId    string   `json:"tenant_id"`
	CdTenantIds []string `json:"cd_tenant_ids"`
}
