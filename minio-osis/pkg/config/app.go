package config

import (
	"log"

	"github.com/minio/madmin-go"
)

var (
	adminClient *madmin.AdminClient
)

func Init() {

	ac, err := madmin.New("10.110.165.191:32000", "minio", "minioadmin", false)
	if err != nil {
		log.Fatalln(err)
	}

	adminClient = ac
}

func GetAdminClient() *madmin.AdminClient {
	return adminClient
}
