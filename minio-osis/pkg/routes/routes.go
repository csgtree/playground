package routes

import (
	"github.com/gorilla/mux"
	"osis.vmware.com/minio/pkg/controllers"
)

func RegisterRoutes(router *mux.Router) {
	router.HandleFunc("/api/v1/tenants", controllers.GetTenants).Methods("GET")
	router.HandleFunc("/api/v1/tenants/{tenantId}", controllers.HeadTenant).Methods("HEAD")
	router.HandleFunc("/api/v1/tenants/{tenantId}", controllers.PatchTenant).Methods("PATCH")
	router.HandleFunc("/api/v1/tenants", controllers.CreateTenant).Methods("POST")
}
