# README

## APIS
[Object Storage Interoperability Service - VMware API Explorer - VMware {code}](https://developer.vmware.com/apis/1034#/required)

### Tenant
- HEAD tenant
- PATCH tenant
- GET tenants 
- POST tenants
- Query tenant

### User
- Query user
- POST users
- GET users
- GET user
- PATCH user
- DELETE user
- GET canonical user
  
### Credential
- Query credentials
- GET user credentials
- POST user crendentials
- GET credential


### Others
- GET S3 capabilities
- GET info
- GET token