#!/bin/bash

auth="authorization: Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJiMzAzOTAwNi0xYWJiLTRlNWItYmU3My03NDE1NzJjZDJjZmRAYzlmNzVhZjItODk3ZS00ZGFlLWFhZWQtOTY0NjJmYjA2MWUzIiwic3ViIjoicmFjaGVsdyIsImV4cCI6MTcwNjU5MjY4NiwidmVyc2lvbiI6InZjbG91ZF8xLjAiLCJqdGkiOiJkOTUxNGUzZDg1ZjM0MjYxYTc0YWQwZmM2M2QwNzRjZiJ9.M9FD4lAHtAW_6IzeOPPjUuGg3ZfBoxTlrJjAzf7N6s8MllxCTdalaS_rd_lUAhHvpiGOc8YoyYS_yPuluqPthOjNt4WUjes0lKA4cTS5d06GOGrdlPz0rlmIKGukxh_lvyDvSpSUbcKD7Jd0Cvsy03Fc9WfaO1IYLdqbh6nYuqfxrdJyx7sxoEQjJQvnt6O8GxSLyTAc3Ol6RKG7hfVC9i8eLvtUmYQpHpJzfUn15dKrA7FwhZysUMhznTau34rkX3T8rRXmTI2vur7-cH3FwPeSvd36r6a1AFlDBk4KqJW4Qa_tWT4PnazlYxhK0LxQN3HUnn80WM9b2REPT6joDA"


for (( ; ; ))
do
  date=$(date '+%Y-%m-%d %H:%M:%S')
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=Screenshot%25202024-01-24%2520at%252013.35.56%25402x.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=Screenshot%25202024-01-24%2520at%252013.34.48%25402x.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=Screenshot%25202024-01-24%2520at%252013.30.28%25402x.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=Screenshot%25202024-01-25%2520at%252017.52.04%25402x.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=Screenshot%25202024-01-25%2520at%252017.49.56%25402x.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=SFO.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=SLC.jpg' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-163.eng.vmware.com/api/v1/s3/demo-east?versions&delimiter=/&prefix=NYC.png' \
    -H 'authority: apaas-163.eng.vmware.com' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US' \
    -H "$auth"\
    -H 'origin: https://apaas-210.eng.vmware.com' \
    -H 'referer: https://apaas-210.eng.vmware.com/' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-site' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'x-vmware-vcloud-auth-context: ACME' \
    -H 'x-vmware-vcloud-tenant-context: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'x-vmware-vose-user-context: 5abf5dae-43ee-4c0f-a1fb-8ed876f35655' \
    --compressed \
    --insecure ;
  curl 'https://apaas-210.eng.vmware.com/api/query?type=task&format=records&page=1&pageSize=50&filterEncoded=true&filter=startDate=gt=2024-01-28T17:51:46.343Z&sortDesc=startDate&links=true' \
    -H 'Accept: application/*+json;version=39.0.0-alpha' \
    -H 'Accept-Language: en-US,en;q=0.9' \
    -H "$auth"\
    -H 'Connection: keep-alive' \
    -H 'Content-Type: application/*+json' \
    -H 'Cookie: vcloud_jwt=eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJiMzAzOTAwNi0xYWJiLTRlNWItYmU3My03NDE1NzJjZDJjZmRAYzlmNzVhZjItODk3ZS00ZGFlLWFhZWQtOTY0NjJmYjA2MWUzIiwic3ViIjoicmFjaGVsdyIsImV4cCI6MTcwNjU5MjY4NiwidmVyc2lvbiI6InZjbG91ZF8xLjAiLCJqdGkiOiJkOTUxNGUzZDg1ZjM0MjYxYTc0YWQwZmM2M2QwNzRjZiJ9.M9FD4lAHtAW_6IzeOPPjUuGg3ZfBoxTlrJjAzf7N6s8MllxCTdalaS_rd_lUAhHvpiGOc8YoyYS_yPuluqPthOjNt4WUjes0lKA4cTS5d06GOGrdlPz0rlmIKGukxh_lvyDvSpSUbcKD7Jd0Cvsy03Fc9WfaO1IYLdqbh6nYuqfxrdJyx7sxoEQjJQvnt6O8GxSLyTAc3Ol6RKG7hfVC9i8eLvtUmYQpHpJzfUn15dKrA7FwhZysUMhznTau34rkX3T8rRXmTI2vur7-cH3FwPeSvd36r6a1AFlDBk4KqJW4Qa_tWT4PnazlYxhK0LxQN3HUnn80WM9b2REPT6joDA; vcloud_session_id=d9514e3d85f34261a74ad0fc63d074cf; _pk_id.1.00e8=4fad56432bb69d7b.1705289763.8.1706506349.1706506296..5a341efd6c7933e97e11275d4b8419cedeacac9ab5ec247236b2c25796372171; _pk_ses.1.00e8=0' \
    -H 'Referer: https://apaas-210.eng.vmware.com/tenant/ACME/plugins/Vk13YXJl/oss' \
    -H 'Sec-Fetch-Dest: empty' \
    -H 'Sec-Fetch-Mode: cors' \
    -H 'Sec-Fetch-Site: same-origin' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36' \
    -H 'X-VMWARE-VCLOUD-AUTH-CONTEXT: ACME' \
    -H 'X-VMWARE-VCLOUD-TENANT-CONTEXT: b3039006-1abb-4e5b-be73-741572cd2cfd' \
    -H 'X-vCloud-Authorization: ACME' \
    -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    --compressed \
    --insecure
  sleep 0.2
done
