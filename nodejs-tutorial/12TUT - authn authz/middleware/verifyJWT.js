const jwt = require("jsonwebtoken");
require("dotenv").config();

const verifyJWT = (req, rsp, next) => {
	const authHeader = req.headers.authorization || req.headers.Authorization;
	// has auth header and starts with Bearer
	if (!authHeader?.startsWith("Bearer")) {
		return rsp.sendStatus(401);
	}

	console.log(authHeader);
	const token = authHeader.split(" ")[1];
	jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
		if (err) {
			return rsp.sendStatus(403);
		}
		req.user = decoded.UserInfo.username;
		req.roles = decoded.UserInfo.roles;
		next();
	});
};

module.exports = verifyJWT;
