const userDB = {
	users: require("../model/users.json"),
	setUsers: function (data) {
		this.users = data;
	},
};

const jwt = require("jsonwebtoken");
require("dotenv");
const path = require("path");

const handleRefreshToken = (req, rsp) => {
	const cookies = req.cookies;
	if (!cookies?.jwt) return rsp.sendStatus(401);

	console.log(cookies.jwt);
	const refreshToken = cookies.jwt;

	const foundUser = userDB.users.find((u) => u.refreshToken === refreshToken);
	if (!foundUser) return rsp.sendStatus(403);

	jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, decoded) => {
		if (err || decoded.username !== foundUser.username)
			return rsp.sendStatus(403);

		const roles = Object.values(foundUser.roles);
		const accessToken = jwt.sign(
			{
				UserInfo: {
					username: decoded.username,
					roles: roles,
				},
			},
			process.env.REFRESH_TOKEN_SECRET,
			{ expiresIn: "30s" },
		);

		return rsp.json({ accessToken });
	});
};

module.exports = { handleRefreshToken };
