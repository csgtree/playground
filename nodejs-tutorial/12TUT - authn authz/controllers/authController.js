const userDB = {
	users: require("../model/users.json"),
	setUsers: function (data) {
		this.users = data;
	},
};

const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");
const fsPromises = require("fs").promises;
require("dotenv");
const path = require("path");

const handleLogin = async (req, rsp) => {
	const { username, pwd } = req.body;
	if (!username || !pwd)
		return rsp
			.status(400)
			.json({ message: "Username and password are required." });

	// check for dup username in the db
	const foundUser = userDB.users.find((u) => u.username === username);
	if (!foundUser) return rsp.sendStatus(401);

	const match = await bcrypt.compare(pwd, foundUser.password);
	if (match) {
		roles = Object.values(foundUser.roles);
		// create JWT
		const accessToken = jwt.sign(
			{
				UserInfo: {
					username: foundUser.username,
					roles: roles,
				},
			},
			process.env.ACCESS_TOKEN_SECRET,
			{ expiresIn: "30s" },
		);
		const refreshToken = jwt.sign(
			{
				username: foundUser.username,
			},
			process.env.REFRESH_TOKEN_SECRET,
			{ expiresIn: "1d" },
		);

		// saving refresh token with current user
		const otherUsers = userDB.users.filter(
			(u) => u.username !== foundUser.username,
		);
		const currentUser = { ...foundUser, refreshToken };
		userDB.setUsers([...otherUsers, currentUser]);
		await fsPromises.writeFile(
			path.join(__dirname, "..", "model", "users.json"),
			JSON.stringify(userDB.users),
		);

		rsp.cookie("jwt", refreshToken, {
			httpOnly: true, // make cookie not available to JS
			maxAge: 24 * 60 * 60 * 1000,
		});
		rsp.json({
			// client should only keep accessToken in memory
			accessToken,
		});
	} else {
		rsp.sendStatus(401);
	}
};

module.exports = { handleLogin };
