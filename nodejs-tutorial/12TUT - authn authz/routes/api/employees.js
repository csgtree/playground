const express = require("express");
const router = express.Router();
const employeesController = require("../../controllers/employeesController");
const ROLES_LIST = require("../../config/roles_list");
const verifyRoles = require("../../middleware/verifyRoles");

router
	.route("/")
	.get(employeesController.getAllEmployees) // apply middleware only to this api, or add to server.js with app.use
	.post(
		verifyRoles(ROLES_LIST.Admin, ROLES_LIST.Editor), // this function return a function (req,rsp, next), which is exptected here
		employeesController.createNewEmployee,
	)
	.put(
		verifyRoles(ROLES_LIST.Admin, ROLES_LIST.Editor),
		employeesController.updateEmployee,
	)
	.delete(verifyRoles(ROLES_LIST.Admin), employeesController.deleteEmployee);

router.route("/:id").get(employeesController.getEmployee);

module.exports = router;
