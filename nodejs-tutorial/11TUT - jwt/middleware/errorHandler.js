const { logEvents } = require('./logEvents')
const errorHandler = (err, req, rsp, next) => {
  logEvents(`${err.name}: ${err.message}`, 'error.log')
  console.log(err.stack);
  rsp.status(500).send(err.message);
}

module.exports = errorHandler
