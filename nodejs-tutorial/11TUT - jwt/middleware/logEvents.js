const { format } = require('date-fns');
const { v4: uuid } = require('uuid');

const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path')


// define async func
const logEvents = async (message, logName) => {
  const dateTime = `${format(new Date(), 'yyyyMMdd\tHH:mm:ss')}`;
  const logItem = `${dateTime}\t${uuid()}\t${message}\n`;
  console.log(logItem)

  try {
    // append will create file but the folder
    // .. can be used in join
    if (!fs.existsSync(path.join(__dirname, '..', 'logs'))) {
      await fsPromises.mkdir(path.join(__dirname, '..', 'logs'));
    }
    await fsPromises.appendFile(path.join(__dirname, '..', 'logs', logName), logItem);
  } catch (err) {
    console.error(err)
  }
}

// middleware to export
const logger = (req, rsp, next) => {
  logEvents(`${req.method}\t${req.path}\t${req.headers.origin}`, 'request.log')
  console.log(`${req.method} ${req.path}`)
  next()
}
// export the func
module.exports = { logEvents, logger } 
