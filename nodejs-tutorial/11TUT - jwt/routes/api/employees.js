const express = require('express');
const router = express.Router();
const employeesController = require('../../controllers/employeesController');
const verifyJWT = require('../../middleware/verifyJWT');



router.route('/')
  .get(verifyJWT, employeesController.getAllEmployees)// apply middleware only to this api, or add to server.js with app.use
  .post(employeesController.createNewEmployee)
  .put(employeesController.updateEmployee)
  .delete(employeesController.deleteEmployee);

router.route('/:id')
  .get(employeesController.getEmployee);

module.exports = router
