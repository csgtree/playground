const express = require("express");
const app = express();
const path = require("path");
const PORT = process.env.PORT | 3500;

// https://expressjs.com/en/guide/routing.html
// app.get("/", (req, rsp) => {
// reg exp for path
app.get("^/$|index(.html)?", (req, rsp) => {
  // rsp.send("Hello World!");

  // specify root folder and the path to the file
  // rsp.sendFile("./views/index.html", { root: __dirname });
  rsp.sendFile(path.join(__dirname, "views", "index.html"));
});

app.get("/new-page(.html)?", (req, rsp) => {
  rsp.sendFile(path.join(__dirname, "views", "new-page.html"));
});

app.get("/old-page(.html)?", (req, rsp) => {
  // 302 by default
  rsp.redirect(301, "/new-page.html");
});

// route handlers
app.get(
  "/hello(.html)?",
  (req, rsp, next) => {
    console.log("This is the first handler.");
    next();
  },
  (req, rsp) => {
    rsp.send("Hello World!");
  },
);

// chaining route handlers
one = (req, rsp, next) => {
  console.log("one");
  next();
};
two = (req, rsp, next) => {
  console.log("two");
  next();
};
three = (req, rsp) => {
  console.log("three");
  rsp.send("done");
};
app.get("/chain", [one, two, three]);

// default in the waterfall
app.get("/*", (req, rsp) => {
  rsp.status(404).sendFile(path.join(__dirname, "views", "404.html"));
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
