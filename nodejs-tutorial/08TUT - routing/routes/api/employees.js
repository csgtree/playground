const express = require('express');
const router = express.Router();

const data = {}
data.employees = require('../../data/employees.json')

router.route('/')
  .get((req, rsp) => {
    rsp.json(data.employees)
  })
  .post((req, rsp) => {
    rsp.json({
      firstname: req.body.firstname,
      lastname: req.body.lastname
    })
  })
  .put((req, rsp) => {
    rsp.json({
      firstname: req.body.firstname,
      lastname: req.body.lastname
    })
  })
  .delete((req, rsp) => {
    rsp.json({
      id: req.body.id
    })
  });

router.route('/:id')
  .get((req, rsp) => {
    rsp.json({
      id: req.params.id
    })
  });

module.exports = router
