const express = require('express');
const router = express.Router();
const path = require('path');

router.get("^/$|index(.html)?", (req, rsp) => {
  rsp.sendFile(path.join(__dirname, "..", "views", "index.html"));
});

router.get("/new-page(.html)?", (req, rsp) => {
  rsp.sendFile(path.join(__dirname, "..", "views", "new-page.html"));
});

router.get("/old-page(.html)?", (req, rsp) => {
  rsp.redirect(301, "/new-page.html");
});

module.exports = router
