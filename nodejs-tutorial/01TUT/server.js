
// NodeJS is js runtime engine
// NodeJS vs vanillia JS
// 1) NodeJS runs on server; it is backend
// 2) the console is the terminal window
console.log("hello world")
// 3) global object instead of window object
// console.log(global);
// 4) has Common Core modules
// 5) CommonJS modules instead of ES6 modules
// 6) missing some JS APIs like fetch

const os = require('os')
const path = require('path')

console.log(os.type())
console.log(os.version())
console.log(os.homedir())

console.log(__dirname)
console.log(__filename)

console.log(path.dirname(__filename))
console.log(path.basename(__filename))
console.log(path.extname(__filename))

console.log(path.parse(__filename))

const math = require('./math')
const { add, subtract, multiply, devide } = require('./math')

console.log(math.add(1, 2))
console.log(add(1, 2))
console.log(subtract(1, 2))
console.log(multiply(1, 2))
console.log(devide(1, 2))
