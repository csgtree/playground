const express = require("express");
const cors = require('cors')
const app = express();
const path = require("path");
const PORT = process.env.PORT | 3500;
const corsOptions = require('./config/corsOptions')

// ./ is mandatory
const { logger, logEvents } = require('./middleware/logEvents')
const errorHandler = require('./middleware/errorHandler')
// use func not accept reg exp and used for middleware
// all func is used for routing and accept reg exp
app.use(logger)

app.use(cors(corsOptions))

// built-in middleware to handle urlencoded data
// in other words, form data:
// 'content-type: application/x-www-form-urlencoded'
app.use(express.urlencoded({ extended: false }));

// built-in middleware for json
app.use(express.json());

// serve static resources
// the resources in public folder can be accessed via root path /
app.use('/', express.static(path.join(__dirname, 'public')))
// use the middleware for the path

// routes
// use the router for the path
app.use('/', require('./routes/root'))
// api
app.use('/employees', require('./routes/api/employees'))


// https://expressjs.com/en/guide/routing.html
// app.get("/", (req, rsp) => {
// reg exp for path


// default in the waterfall
// all func used for routing and accept reg exp, also apply for all http methods at once
app.all("*", (req, rsp) => {
  rsp.status(404);
  if (req.accepts('html')) {
    rsp.sendFile(path.join(__dirname, "views", "404.html"));
  } else if (req.accepts('json')) {
    rsp.json({ error: '404 Not Found' });
  } else {
    rsp.type('txt').send('404 Not Found')
  }
});

// custom error handling
// app.use((err, req, rsp, next) => {
//   console.log(err.stack);
//   rsp.status(500).send(err.message);
// })
app.use(errorHandler)

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
