// console.log('testing!')

// install a package
// monitor js without run it again and again
// npm i nodemon -g
// nodemon

// create a package
// npm init 

// add dependency for package
// npm i date-fns
// modify package.json and include folder node_modules
// node_modules should be ignored for git as it is huge; only package.json should be included

// save dev dependency
// npm i nodemon -D
// TODO what is dev dependency? tool dependency instead of runtime dependency

// package.json -- scripts - start/dev
// npm start
// npm run dev
const { format } = require('date-fns');
console.log(format(new Date(), 'yyyyMMdd\tHH:mm:ss'))

console.log('hello')

// import specific version
const { v4: uuid } = require('uuid');
console.log(uuid())
console.log(uuid())

// package.json -- dependencies
// uuid version "^8.3.2", major/minor/patch
// "^" means not to update major version

// uuid version "~8.3.2", major/minor/patch
// "~" means not to update major/minor version

// uuid version "*"
// "*" means to update the latest version at anytime

// install specific version
// npm i uuid@8.3.2

// update dependencies
// npm update

// uninstall dependency
// npm rm nodemon -D
// npm rm nodemon -g
