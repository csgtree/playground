const userDB = {
  users: require('../model/users.json'),
  setUsers: function(data) { this.users = data }
}

const fsPromises = require('fs').promises;
const path = require('path');
const bcrypt = require('bcrypt');

const handleNewUser = async (req, rsp) => {
  const { username, pwd } = req.body;
  if (!username || !pwd) return rsp.status(400).json(
    { message: 'Username and password are required.' }
  )

  // check for dup username in the db
  const dup = userDB.users.find(u => u.username === username);
  if (dup) return rsp.sendStatus(409);
  try {
    // encrypt pwd
    const hashedPwd = await bcrypt.hash(pwd, 10);

    // store the new user
    const newUser = {
      username: username,
      password: hashedPwd
    };
    userDB.setUsers([...userDB.users, newUser]);

    await fsPromises.writeFile(
      path.join(__dirname, '..', 'model', 'users.json'),
      JSON.stringify(userDB.users)
    );
    console.log(userDB.users);

    rsp.status(201).json({ message: `New user ${username} created!` })
  } catch (err) {
    rsp.status(500).json({ message: err.message });
  }

}

module.exports = { handleNewUser }
