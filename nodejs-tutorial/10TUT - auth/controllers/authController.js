const userDB = {
  users: require('../model/users.json'),
  setUsers: function(data) { this.users = data }
}

const bcrypt = require('bcrypt');

const handleLogin = async (req, rsp) => {
  const { username, pwd } = req.body;
  if (!username || !pwd) return rsp.status(400).json(
    { message: 'Username and password are required.' }
  )

  // check for dup username in the db
  const foundUser = userDB.users.find(u => u.username === username);
  if (!foundUser) return rsp.sendStatus(401);

  const match = await bcrypt.compare(pwd, foundUser.password);
  if (match) {
    // create JWT 
    rsp.json(
      { success: `${username} is logged in.` }
    )
  } else {
    rsp.sendStatus(401);
  }

}

module.exports = { handleLogin }
