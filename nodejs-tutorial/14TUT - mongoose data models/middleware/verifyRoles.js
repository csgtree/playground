const verifyRoles = (...allowedRoles) => {
	// return is mandatory here. it is a function with arg allowedRoles and return a function (req, rsp, next)
	return (req, rsp, next) => {
		if (!req?.roles) return rsp.sendStatus(401);

		const rolesArray = [...allowedRoles];
		console.log(rolesArray);
		console.log(req.roles);

		// any role in jwt is in the allowed roles
		const result = req.roles
			.map((role) => rolesArray.includes(role))
			.find((v) => v === true);

		if (!result) return rsp.sendStatus(401);

		next();
	};
};

module.exports = verifyRoles;
