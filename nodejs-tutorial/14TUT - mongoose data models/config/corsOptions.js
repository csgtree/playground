const whitelist = [
  'https://www.foobar.com',
  'https://www.google.com',
  'http://localhost:3500'
];

const corsOptions = {
  origin: (origin, callback) => {
    if (!origin || whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'), false)
    }
  },
  optionSuccessStatus: 200
}

module.exports = corsOptions
