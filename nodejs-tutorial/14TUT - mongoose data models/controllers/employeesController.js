// const data = {}
// data.employees = require('../model/employees.json')
const data = {
  employees: require('../model/employees.json'),
  setEmployees: function(data) {
    this.employees = data
  }

}
const getAllEmployees = (req, rsp) => {
  rsp.json(data.employees);
}

const createNewEmployee = (req, rsp) => {
  const newEmployee = {
    id: data.employees[data.employees.length - 1].id + 1 || 1,
    firstname: req.body.firstname,
    lastname: req.body.lastname
  }

  if (!newEmployee.firstname || !newEmployee.lastname) {
    return rsp.status(400).json({ message: 'First and last names are required.' })
  }

  // use [] for array
  data.setEmployees([...data.employees, newEmployee])
  rsp.status(201).json(data.employees)
}

const updateEmployee = (req, rsp) => {
  const employee = data.employees.find(e => e.id === parseInt(req.body.id));
  // if not exist, return 404
  if (!employee) {
    return rsp.status(400).json({ message: `Employee ID ${req.body.id} not found` })
  }

  // update value
  if (req.body.firstname) employee.firstname = req.body.firstname;
  if (req.body.lastname) employee.lastname = req.body.lastname;

  // extract employees other than the one to udpate
  const filteredArray = data.employees.filter(e => e.id !== parseInt(req.body.id));

  // generate full employees list
  const unsortedArray = [...filteredArray, employee]
  console.log('wangc...unsortedArray', unsortedArray)

  // sort 
  data.setEmployees(unsortedArray.sort((a, b) => {
    // not missing this return
    return a.id > b.id
      ? 1
      : a.id < b.id
        ? -1
        : 0
  }));
  console.log('wangc...', data.employees)

  // return 
  rsp.json(data.employees);
}

const deleteEmployee = (req, rsp) => {
  const employee = data.employees.find(e => e.id === parseInt(req.body.id));
  // if not exist, return 404
  if (!employee) {
    return rsp.status(204).json({ message: `Employee ID ${req.body.id} not found` })
  }


  // extract employees other than the one to udpate
  const filteredArray = data.employees.filter(e => e.id !== parseInt(req.body.id));

  // generate full employees list
  data.setEmployees([...filteredArray]);

  rsp.json(data.employees)
}

const getEmployee = (req, rsp) => {
  const employee = data.employees.find(e => e.id === parseInt(req.params.id));
  // if not exist, return 404
  if (!employee) {
    return rsp.status(404).json({ message: `Employee ID ${req.body.id} not found` })
  }
  rsp.json(employee)
};

module.exports = {
  getAllEmployees,
  createNewEmployee,
  updateEmployee,
  deleteEmployee,
  getEmployee
}
