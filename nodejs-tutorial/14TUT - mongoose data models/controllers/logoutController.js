const userDB = {
	users: require("../model/users.json"),
	setUsers: function (data) {
		this.users = data;
	},
};

const fsPromises = require("fs").promises;
const path = require("path");

const handleLogout = async (req, rsp) => {
	// on client, access token should be deleted
	const cookies = req.cookies;
	if (!cookies?.jwt) return rsp.sendStatus(204);

	console.log(cookies.jwt);
	const refreshToken = cookies.jwt;

	// is refreshToken in DB
	const foundUser = userDB.users.find((u) => u.refreshToken === refreshToken);
	if (!foundUser) {
		// clear cookie
		rsp.clearCookie("jwt", { httpOnly: true });
		return rsp.sendStatus(204);
	}

	// delete refreshToken in DB
	const otherUsers = userDB.users.filter(
		(u) => u.refreshToken !== refreshToken,
	);
	const currentUser = { ...foundUser, refreshToken: "" };

	userDB.setUsers([...otherUsers, currentUser]);

	await fsPromises.writeFile(
		path.join(__dirname, "..", "model", "users.json"),
		JSON.stringify(userDB.users),
	);
	rsp.clearCookie("jwt", {
		httpOnly: true,
		maxAge: 24 * 60 * 60 * 1000,
	}); // secure: true - only serve on https
	rsp.sendStatus(204);
};

module.exports = { handleLogout };
