const http = require('http');
const path = require('path');
const fs = require('fs');
const fsPromises = require('fs').promises;

const logEvents = require('./logEvents');
const EventEmitter = require('events');


// emit and listener events
class Emitter extends EventEmitter { };

// init object 
const myEmitter = new Emitter();
myEmitter.on('log', (msg, fileName) => {
  logEvents(msg, fileName)
})


const PORT = process.env.PORT | 3500;
const serveFile = async (filePath, contentType, rsp) => {
  try {
    const rawData = await fsPromises.readFile(
      filePath,
      !contentType.includes('image') ? 'utf8' : ''
    );
    const data = contentType === 'application/json' ? JSON.stringify(JSON.parse(rawData)) : rawData;
    rsp.writeHead(
      filePath.includes('404.html') ? 404 : 200,
      'Content-Type', contentType);
    rsp.end(data);
  } catch (err) {
    console.log(err);
    myEmitter.emit('log', `${err.name}: ${err.message}`, 'errLog.log')
    rsp.statusCode = 500;
    rsp.end();
  }
}


const server = http.createServer((req, rsp) => {
  console.log(req.url, req.method)
  myEmitter.emit('log', `${req.url}\t${req.method}`, 'reqLog.log')

  // let filePath;
  // if (req.url === '/' || req.url === 'index.html') {
  //   rsp.statusCode = 200
  //   rsp.setHeader('Content-Type', 'text/html');
  //   filePath = path.join(__dirname, 'views', 'index.html')
  //   fs.readFile(filePath, 'utf8', (err, data) => {
  //     rsp.end(data)
  //   })

  extension = path.extname(req.url);

  let contentType;

  switch (extension) {
    case '.css':
      contentType = 'text/css'
      break;
    case '.js':
      contentType = 'text/javascript'
      break;
    case '.json':
      contentType = 'application/json'
      break;
    case '.jpg':
      contentType = 'image/jpeg'
      break;
    case '.png':
      contentType = 'image/png'
      break;
    case '.txt':
      contentType = 'text/plain'
      break;
    default:
      contentType = 'text/html'

  }

  let filePath =
    contentType === 'text/html' && req.url === '/'
      ? path.join(__dirname, 'views', 'index.html')
      : contentType === 'text/html' && req.url.slice(-1) === '/'
        ? path.join(__dirname, 'views', req.url, 'index.html')
        : contentType === 'text/html'
          ? path.join(__dirname, 'views', req.url)
          : path.join(__dirname, req.url);

  // makes .html extension not required in browser
  if (!extension && req.url.slice(-1) !== '/') {
    filePath += '.html'
  }

  const fileExists = fs.existsSync(filePath);

  if (fileExists) {
    serveFile(filePath, contentType, rsp);
    // fs.readFile(filePath, 'utf8', (err, data) => {
    //   rsp.end(data);
    // })
  } else {
    switch (path.parse(filePath).base) {
      case 'old-page.html':
        rsp.writeHead('301', { 'Location': '/new-page.html' });
        rsp.end();
        break;
      case 'www-page.html':
        rsp.writeHead('301', { 'Location': '/' });
        rsp.end();
        break;
      default:
        serveFile(path.join(__dirname, 'views', '404.html'), 'text/html', rsp);
    }
  }
})
server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})


