const express = require("express");
const cors = require('cors')
const app = express();
const path = require("path");
const PORT = process.env.PORT | 3500;

// ./ is mandatory
const { logger, logEvents } = require('./middleware/logEvents')
const errorHandler = require('./middleware/errorHandler')

// middleware in express is something like filer/interceptor in Spring

// custom middleware logger
// next is mandatory in custom middleware
// app.use((req, rsp, next) => {
//   logEvents(`${req.method}\t${req.path}\t${req.headers.origin}`, 'express.log')
//   console.log(`${req.method} ${req.path}`)
//   next()
// })

// use func not accept reg exp and used for middleware
// all func is used for routing and accept reg exp
app.use(logger)

// apply cors middleware
const whitelist = ['https://www.foobar.com',
  'https://www.google.com',
  'http://localhost:3500'
]
const corsOptions = {
  origin: (origin, callback) => {
    if (!origin || whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'), false)
    }
  },
  optionSuccessStatus: 200
}
app.use(cors(corsOptions))

// built-in middleware to handle urlencoded data
// in other words, form data:
// 'content-type: application/x-www-form-urlencoded'
app.use(express.urlencoded({ extended: false }));

// built-in middleware for json
app.use(express.json());

// serve static resources
// the resources in public folder can be accessed via root path /
app.use(express.static(path.join(__dirname, 'public')))

// https://expressjs.com/en/guide/routing.html
// app.get("/", (req, rsp) => {
// reg exp for path
app.get("^/$|index(.html)?", (req, rsp) => {
  // rsp.send("Hello World!");

  // specify root folder and the path to the file
  // rsp.sendFile("./views/index.html", { root: __dirname });
  rsp.sendFile(path.join(__dirname, "views", "index.html"));
});

app.get("/new-page(.html)?", (req, rsp) => {
  rsp.sendFile(path.join(__dirname, "views", "new-page.html"));
});

app.get("/old-page(.html)?", (req, rsp) => {
  // 302 by default
  rsp.redirect(301, "/new-page.html");
});

// route handlers
// route handler is kind of middleware
app.get(
  "/hello(.html)?",
  (req, rsp, next) => {
    console.log("This is the first handler.");
    next();
  },
  (req, rsp) => {
    rsp.send("Hello World!");
  },
);

// chaining route handlers
one = (req, rsp, next) => {
  console.log("one");
  next();
};
two = (req, rsp, next) => {
  console.log("two");
  next();
};
three = (req, rsp) => {
  console.log("three");
  rsp.send("done");
};
app.get("/chain", [one, two, three]);

// default in the waterfall
// all func used for routing and accept reg exp, also apply for all http methods at once
app.all("*", (req, rsp) => {
  rsp.status(404);
  if (req.accepts('html')) {
    rsp.sendFile(path.join(__dirname, "views", "404.html"));
  } else if (req.accepts('json')) {
    rsp.json({ error: '404 Not Found' });
  } else {
    rsp.type('txt').send('404 Not Found')
  }
});

// custom error handling
// app.use((err, req, rsp, next) => {
//   console.log(err.stack);
//   rsp.status(500).send(err.message);
// })
app.use(errorHandler)

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
