// const fs = require('fs');
const path = require('path');
const fsPromises = require('fs').promises;

// fs.readFile('./files/baggio.txt', 'utf8', (err, data) => {
// fs.readFile(path.join(__dirname, 'files', 'baggio.txt'), 'utf8', (err, data) => {
//   if (err) throw err
//   console.log(data.toString())
// })

// async 
// console.log('hello...')


// fs.writeFile(path.join(__dirname, 'files', 'messi.txt'), 'Football goat.', (err) => {
//   if (err) throw err
//   console.log('Write complete')
// })

// callback hell
// fs.writeFile(path.join(__dirname, 'files', 'romario.txt'), 'Best forward.', (err) => {
//   if (err) throw err
//   console.log('Write complete')
//
//
//   fs.appendFile(path.join(__dirname, 'files', 'romario.txt'), '\n5 goals in US World Cup.', (err) => {
//     if (err) throw err
//     console.log('Append complete')
//
//     fs.rename(path.join(__dirname, 'files', 'romario.txt'), path.join(__dirname, 'files', 'romario-the-wolf.txt'), (err) => {
//       if (err) throw err
//       console.log('Rename complete')
//     })
//   })
// })

// fs.readFile('./files/not-exist.txt', 'utf8', (err, data) => {
//   if (err) throw err
//   console.log(data.toString())
// })

const fileOps = async () => {
  try {
    const data = await fsPromises.readFile(path.join(__dirname, 'files', 'baggio.txt'), 'utf8')
    console.log(data.toString())

    // unlink is delete
    await fsPromises.unlink(path.join(__dirname, 'files', 'messi.txt'))

    await fsPromises.writeFile(path.join(__dirname, 'files', 'new-italy-star.txt'), data)
    await fsPromises.appendFile(path.join(__dirname, 'files', 'new-italy-star.txt'), '\nBaggio junior.')
    await fsPromises.rename(path.join(__dirname, 'files', 'new-italy-star.txt'), path.join(__dirname, 'files', 'Baggio junior.txt'))

    const newData = await fsPromises.readFile(path.join(__dirname, 'files', 'Baggio junior.txt'), 'utf8')
    console.log(newData.toString())
  } catch (err) {
    console.error(err)
  }
}

fileOps()

// exit on uncaught error
process.on('uncaughtException', err => {
  console.log(`There was an uncaught error: ${err}`)
  process.exit(1)
})
