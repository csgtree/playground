const User = require("../model/User");

const bcrypt = require("bcrypt");

const handleNewUser = async (req, rsp) => {
	const { username, pwd } = req.body;
	if (!username || !pwd)
		return rsp
			.status(400)
			.json({ message: "Username and password are required." });

	// check for dup username in the db
	// const dup = userDB.users.find((u) => u.username === username);
	const dup = await User.findOne({ username: username }).exec();
	if (dup) return rsp.sendStatus(409);
	try {
		// encrypt pwd
		const hashedPwd = await bcrypt.hash(pwd, 10);

		// store the new user
		const result = await User.create({
			username: username,
			password: hashedPwd,
		});

		// another syntax
		// const newUser = new User({
		//
		// });
		// const result = await newUser.save();

		console.log(result);

		rsp.status(201).json({ message: `New user ${username} created!` });
	} catch (err) {
		rsp.status(500).json({ message: err.message });
	}
};

module.exports = { handleNewUser };
