const User = require("../model/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const handleLogin = async (req, rsp) => {
	const { username, pwd } = req.body;
	if (!username || !pwd)
		return rsp
			.status(400)
			.json({ message: "Username and password are required." });

	// check for dup username in the db
	const foundUser = await User.findOne({ username: username }).exec();
	if (!foundUser) return rsp.sendStatus(401);

	const match = await bcrypt.compare(pwd, foundUser.password);
	if (match) {
		roles = Object.values(foundUser.roles);
		// create JWT
		const accessToken = jwt.sign(
			{
				UserInfo: {
					username: foundUser.username,
					roles: roles,
				},
			},
			process.env.ACCESS_TOKEN_SECRET,
			{ expiresIn: "300s" },
		);
		const refreshToken = jwt.sign(
			{
				username: foundUser.username,
			},
			process.env.REFRESH_TOKEN_SECRET,
			{ expiresIn: "1d" },
		);

		// saving refresh token with current user
		foundUser.refreshToken = refreshToken;
		const result = await foundUser.save();
		console.log(result);
		rsp.cookie("jwt", refreshToken, {
			httpOnly: true, // make cookie not available to JS
			maxAge: 24 * 60 * 60 * 1000,
		});
		rsp.json({
			// client should only keep accessToken in memory
			accessToken,
		});
	} else {
		rsp.sendStatus(401);
	}
};

module.exports = { handleLogin };
