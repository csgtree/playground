const User = require("../model/User");

const handleLogout = async (req, rsp) => {
	// on client, access token should be deleted
	const cookies = req.cookies;
	if (!cookies?.jwt) return rsp.sendStatus(204);

	console.log(cookies.jwt);
	const refreshToken = cookies.jwt;

	// is refreshToken in DB
	const foundUser = await User.findOne({ refreshToken: refreshToken }).exec();
	if (!foundUser) {
		// clear cookie
		rsp.clearCookie("jwt", { httpOnly: true });
		return rsp.sendStatus(204);
	}

	// delete refreshToken in DB
	foundUser.refreshToken = "";
	const result = await foundUser.save();
	console.log(result);

	rsp.clearCookie("jwt", {
		httpOnly: true,
		maxAge: 24 * 60 * 60 * 1000,
	}); // secure: true - only serve on https
	rsp.sendStatus(204);
};

module.exports = { handleLogout };
