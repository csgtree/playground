const Employee = require("../model/Employee");

const getAllEmployees = async (req, rsp) => {
    const employees = await Employee.find();
    if (!employees) return rsp.status(204).json({ message: "No employee found" });
    rsp.json(employees);
};

const createNewEmployee = async (req, rsp) => {
    if (!req?.body?.firstname || !req?.body?.lastname) {
        return rsp
            .status(400)
            .json({ message: "First and  last name are required" });
    }

    try {
        const ret = await Employee.create({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
        });

        return rsp.status(201).json(ret);
    } catch (err) {
        console.log(err);
    }
};

const updateEmployee = async (req, rsp) => {
    if (!req?.body?.id) {
        return rsp.status(400).json({ message: "ID parameter is required" });
    }

    const employee = await Employee.findOne({ _id: req.body.id }).exec();
    // if not exist, return 404
    if (!employee) {
        return rsp
            .status(204)
            .json({ message: `Employee ID ${req.body.id} not found` });
    }

    // update value
    if (req.body?.firstname) employee.firstname = req.body.firstname;
    if (req.body?.lastname) employee.lastname = req.body.lastname;

    const ret = await employee.save();
    // return
    rsp.json(ret);
};

const deleteEmployee = async (req, rsp) => {
    if (!req?.body?.id) {
        return rsp.status(400).json({ message: "ID parameter is required" });
    }

    const employee = await Employee.findOne({ _id: req.body.id }).exec();
    // if not exist, return 404
    if (!employee) {
        return rsp
            .status(204)
            .json({ message: `Employee ID ${req.body.id} not found` });
    }

    const ret = await employee.deleteOne({ _id: req.body.id });
    rsp.json(ret);
};

const getEmployee = async (req, rsp) => {
    if (!req?.params?.id) {
        return rsp.status(400).json({ message: "ID parameter is required" });
    }
    const employee = await Employee.findOne({ _id: req.params.id }).exec();

    // if not exist, return 404
    if (!employee) {
        return rsp
            .status(204)
            .json({ message: `Employee ID ${req.body.id} not found` });
    }
    rsp.json(employee);
};

module.exports = {
    getAllEmployees,
    createNewEmployee,
    updateEmployee,
    deleteEmployee,
    getEmployee,
};
