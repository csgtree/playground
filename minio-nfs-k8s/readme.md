# Readme

Install NFS Provisioner
```bash
helm install nfs-provisioner ./ -f ./new-values.yaml
```

Install MinIO
```bash
helm install myminio ./ --namespace minio -f new-values.yaml
```
