 Tmux

## session
new session
`tmux`
`tmux new -s {session-name}`

attach session
`tmux a -t {session-name}`

list sessions
`tmux ls`
`prefix s`

kill session
`tmux kill-ses -t {session-name} -a`, a means except the specific session

detach session
`prefix d`

rename session
`prefix $`

## window
new window
`prefix c`

list windows
`prefix w`

navigate windows
`shift arrows`

kill window
`prefix &`

rename window
`prefix ,`

## pane
new pane
`prefix v|h`

zoom toggle pane
`prefix z`

navigate panes
`alt arrows`

kill pane
`prefix x`

sync panes
`C-a C-y` to sync the panels
