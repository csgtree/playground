# Cheat Sheet
[Why I love Vim: It’s the lesser-known features that make it so amazing](https://www.freecodecamp.org/news/learn-linux-vim-basic-features-19134461ab85/)


## Cursor Movement
`{` and `}`, jump block of code
num + `j`, move down num lines
num + `{` and `}`, move num blocks
`zz`, center cursor on screen
`^`, jump to the first non-blank character of the line
`w` and `b`, move forward and backward a word
`W` and `B`, word can has punctuation
`t` char, go to left of specific char
`f` char, go to the specific char
`;` - repeat previous f, t, F or T movement
`%` - move to matching character
`:`num, go to line num

## Editing
`u`, undo
ctrl `r`, redo
`.`, repeat last command
`C`, delete to the end of the line and insert mode
`cw`, change the word, delete the word and insert mode
`ct`char, delete up to char and insert mode
`o`, new line and insert mode
`O`, insert above
`~`, toggle case
`>`, indent
`>>`, indent line
num `r` char, replace num chars with char
`0` then `C` blahblah ., 若干行进行相同修改


## Cut and Paste
`yy`, copy line
`p`, paste below
`P`, paste above
`dd `and `p`, cut and paste
num `x`, delete num chars
num `dd`, delete num lines
`D`, delete (cut) to the end of the line 
`dw`, delete the word from the cursor
`diw `- delete (cut) word under the cursor
`daw `- delete (cut) word under the cursor and the space after or before it
`d2w`, delete 2 words
`d`, delete selected lines
`d%`, delete all between the matching chars
`d` any move command
`y`, copy

## Visual Mode
`V`, visual line mode to select
`v`, visual char mode to select

## Macro
`qd`, start recording to register d
..., your complex series of commands
`q`, stop recording
`@d`, execute your macro
`@@`, execute your macro again

`20@d`, execute macro 20 times

## Others
ctrl `v`, column select
`*`,  match next
