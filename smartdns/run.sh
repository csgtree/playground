#!/bin/bash

docker run -d --name smartdns --restart=always -p 53:53/udp -v `pwd`/:/etc/smartdns pymumu/smartdns:latest
